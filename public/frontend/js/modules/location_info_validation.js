
      function location_info_validation(e){
            e.preventDefault();
            var state_id = document.getElementsByName("state_id");
            var city_id = document.getElementsByName("city_id");
            var locality = document.getElementsByName("locality");
            var prop_addrs = document.getElementsByName("property_address");
            

            var selectedState = $(state_id).children("option:selected").val();
            var selectedCity = $(city_id).children("option:selected").val();
            var locality_det = $(locality).val();
            var prop_addrs_det = $(prop_addrs).val();
           
            
            var state_id_valid = false;
            var city_id_valid = false;
            var locality_valid = false;
            var prop_addrs_valid = false;
            

            // state validation
            
            if (selectedState != '') {
                state_id_valid = true;
            }
                
            if (!state_id_valid )  
            {
                if($("div#state_id > span#state_id_err_msg").length == 0)
                {
                    $("div#state_id").append("<span id='state_id_err_msg' style='color:red;'>Select State</span>");
                }
                $('html, body').animate({
                    scrollTop: $("div#state_id").offset().top
                }, 2000);
                return false;
            }
            else
            {
                if($("div#state_id > span#state_id_err_msg").length > 0)
                {
                    $("div#state_id > span#state_id_err_msg").remove();
                }
                state_id_valid = true;
            }

            // city validation

            if (selectedCity != '') {
                city_id_valid = true;
            }
                
            if (!city_id_valid )  
            {
                if($("div#city_id > span#city_id_err_msg").length == 0)
                {
                    $("div#city_id").append("<span id='city_id_err_msg' style='color:red;'>Select City</span>");
                }
                $('html, body').animate({
                    scrollTop: $("div#city_id").offset().top
                }, 2000);
                return false;
            }
            else
            {
                if($("div#city_id > span#city_id_err_msg").length > 0)
                {
                    $("div#city_id > span#city_id_err_msg").remove();
                }
                city_id_valid = true;
            }

            // locality validation

            if (locality_det != '') {
                locality_valid = true;
            }
                
            if (!locality_valid )  
            {
                if($("div#locality > span#locality_err_msg").length == 0)
                {
                    $("div#locality").append("<span id='locality_err_msg' style='color:red;'>Enter Locality</span>");
                }
                $('html, body').animate({
                    scrollTop: $("div#locality").offset().top
                }, 2000);
                return false;
            }
            else
            {
                if($("div#locality > span#locality_err_msg").length > 0)
                {
                    $("div#locality > span#locality_err_msg").remove();
                }
                locality_valid = true;
            }

            // Property address validation

            if (prop_addrs_det != '') {
                prop_addrs_valid = true;
            }
                
            if (!prop_addrs_valid )  
            {
                if($("div#prop_addrs > span#prop_addrs_err_msg").length == 0)
                {
                    $("div#prop_addrs").append("<span id='prop_addrs_err_msg' style='color:red;'>Enter Address</span>");
                }
                $('html, body').animate({
                    scrollTop: $("div#prop_addrs").offset().top
                }, 2000);
                return false;
            }
            else
            {
                if($("div#prop_addrs > span#prop_addrs_err_msg").length > 0)
                {
                    $("div#prop_addrs > span#prop_addrs_err_msg").remove();
                }
                prop_addrs_valid = true;
            }


            
            
            

            if (!prop_addrs_valid || !locality_valid || !city_id_valid || !state_id_valid)  
            {

                return false;
            }
            
            $("li#property-info > a").click();
        }