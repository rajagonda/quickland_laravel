<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->string('property_id')->nullable();

            $table->integer('user_type_id')->nullable();
            $table->text('about_you')->nullable();
            $table->integer('use_type_id')->nullable();
            $table->integer('property_type_id')->nullable();
            $table->integer('property_sub_type_id')->nullable();
            $table->integer('availability_id')->nullable();
            $table->integer('possession_id')->nullable();
            $table->date('possession_date')->nullable();

            $table->string('locality')->nullable();
            $table->integer('state_id')->nullable();
            $table->string('city_id')->nullable();
            $table->string('project_name')->nullable();
            $table->text('property_address')->nullable();


            $table->text('old_property_files')->nullable();
            $table->text('new_property_files')->nullable();
            $table->text('video_url')->nullable();



            $table->string('plot_area')->nullable();
            $table->integer('plot_area_measure_id')->nullable();
            $table->integer('no_of_plots_per_floor')->nullable();
            $table->integer('length')->nullable();
            $table->integer('length_measure_id')->nullable();
            $table->integer('width')->nullable();
            $table->integer('width_measure_id')->nullable();
            $table->integer('width_of_facing')->nullable();
            $table->integer('facing_measure_id')->nullable();
            $table->integer('plot_price')->nullable();
            $table->integer('price_per_sq_feet')->nullable();
            $table->integer('plot_booking_amount')->nullable();
            $table->integer('maintenance_amount')->nullable();
            $table->string('booking_type')->nullable();

            $table->text('about_property')->nullable();
            $table->longText('amenities')->nullable();
            $table->integer('negotiable')->nullable();
            $table->string('package')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
