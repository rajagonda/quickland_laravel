
INSERT INTO `availability` (`id`, `available_for`, `created_at`, `updated_at`) VALUES
(1, 'New', '2019-12-04 13:00:00', '2019-12-25 11:32:41'),
(2, 'Resale', '2019-12-04 13:00:00', '2019-12-21 01:06:04');

INSERT INTO `comm_land_types` (`comm_land_type_id`, `comm_land_type`, `created_at`, `updated_at`) VALUES
(1, 'Commercial Land', '2019-12-03 17:46:02', '2019-12-03 17:46:02'),
(2, 'Agriculture Lands/Farm Land', '2019-12-03 17:49:40', '2019-12-03 17:49:40'),
(3, 'Industrial Lands', '2019-12-03 17:49:59', '2019-12-03 17:49:59');

INSERT INTO `maintenance_types` (`mtype_id`, `mtype`, `created_at`, `updated_at`) VALUES
(1, 'Monthly', '2019-12-03 17:48:14', '2019-12-03 17:48:14');

INSERT INTO `plot_area_measures` (`plot_measure_id`, `plot_measure`, `created_at`, `updated_at`) VALUES
(1, 'Square Feet', '2019-12-21 13:00:00', '2019-12-21 13:00:00'),
(2, 'Square Yard', '2019-12-21 13:00:00', '2019-12-21 13:00:00'),
(3, 'Square Meters', '2019-12-21 13:00:00', '2019-12-21 13:00:00'),
(4, 'Cents', '2019-12-21 13:00:00', '2019-12-21 13:00:00');

INSERT INTO `possessions` (`id`, `possession`, `created_at`, `updated_at`) VALUES
(1, 'Ready to Move', NULL, NULL),
(2, 'Add Date', NULL, NULL);

INSERT INTO `property_sub_types` (`id`, `property_sub_type`, `property_type_id`, `created_at`, `updated_at`) VALUES
(1, 'Residential Individual Plot', 1, NULL, NULL),
(2, 'Residential Project', 1, NULL, NULL);


INSERT INTO `property_types` (`id`, `property_uses_id`, `property_type`, `project_type`, `created_at`, `updated_at`) VALUES
(1, '1', 'Residential', NULL, NULL, NULL),
(2, '1', 'Commercial', NULL, NULL, NULL);


INSERT INTO `property_uses` (`id`, `use_type`, `created_at`, `updated_at`) VALUES
(1, 'Sell', NULL, NULL),
(2, 'Buy', NULL, NULL);



INSERT INTO `roles` (`id`, `role_id`, `role_name`, `role_description`, `created_at`, `updated_at`) VALUES
(1, 'ADMN-1', 'ADMIN', 'Administration', '2020-02-28 20:00:00', '2020-02-28 20:00:00');



INSERT INTO `states` (`id`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Andhra Pradesh', NULL, NULL),
(2, 'Telangana', NULL, NULL);



INSERT INTO `user_roles` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'ADMN-1', 1, '2020-02-28 20:00:00', '2020-02-28 20:00:00');



INSERT INTO `user_types` (`id`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'Owner', '2019-12-03 17:43:49', '2019-12-15 03:51:07'),
(2, 'Agent', '2019-12-04 13:00:00', '2019-12-04 13:00:00'),
(3, 'Builder', '2019-12-04 13:00:00', '2019-12-04 13:00:00');
