const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');


mix.js('resources/assets/frontend/js/site.js', 'public/frontend/js')
    .sass('resources/assets/frontend/scss/site_style.scss', 'public/frontend/css');



mix.copy('resources/js/customScript.js', 'public/js/customScript.js');

mix.copyDirectory('resources/assets/frontend/images', 'public/frontend/images');
mix.copyDirectory('resources/assets/frontend/js/modules', 'public/frontend/js/modules');
// mix.copyDirectory('resources/assets/modules', 'public/modules');