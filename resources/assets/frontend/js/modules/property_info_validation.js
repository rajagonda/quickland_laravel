function property_info_validation(e){

    e.preventDefault();
    var plot_area = document.getElementsByName("plot_area");
    var plot_area_measure_id = document.getElementsByName("plot_area_measure_id");
    var width_of_facing = document.getElementsByName("width_of_facing");
    var plot_price = document.getElementsByName("plot_price");
    var price_per_sq_feet = document.getElementsByName("price_per_sq_feet");


    
    var plot_area_det = $(plot_area).val();
    var selectedPlotAreaMeasure = $(plot_area_measure_id).children("option:selected").val();
    var width_of_facing_det = $(width_of_facing).val();
    var plot_price_det = $(plot_price).val();
    var price_per_sq_feet_det = $(price_per_sq_feet).val();

    var plot_area_valid = false;
    var plot_area_measure_valid = false;
    var width_of_facing_valid = false;
    var plot_price_valid = false;
    var price_per_sq_feet_valid = false;

    

    if (plot_area_det != '') {
        plot_area_valid = true;
    }

    if (!plot_area_valid )  
    {
        if($("div#plot_area > span#plot_area_err_msg").length == 0)
        {
            $("div#plot_area").append("<span id='plot_area_err_msg' style='color:red;'>Enter Plot area</span>");
        }
        $('html, body').animate({
            scrollTop: $("div#plot_area").offset().top
        }, 2000);
        return false;
    }
    else
    {
        if($("div#plot_area > span#plot_area_err_msg").length > 0)
        {
            $("div#plot_area > span#plot_area_err_msg").remove();
        }
        plot_area_valid = true;
    }

    // state validation

    if (selectedPlotAreaMeasure != '') {
        plot_area_measure_valid = true;
    }

    if (!plot_area_measure_valid )  
    {
        if($("div#plot_area_measure > span#plot_area_measure_err_msg").length == 0)
        {
            $("div#plot_area_measure").append("<span id='plot_area_measure_err_msg' style='color:red;'>Select Measure</span>");
        }
        $('html, body').animate({
            scrollTop: $("div#plot_area_measure").offset().top
        }, 2000);
        return false;
    }
    else
    {
        if($("div#plot_area_measure > span#plot_area_measure_err_msg").length > 0)
        {
            $("div#plot_area_measure > span#plot_area_measure_err_msg").remove();
        }
        plot_area_measure_valid = true;
    }


    if (width_of_facing_det != '') {
        width_of_facing_valid = true;
    }

    if (!width_of_facing_valid )  
    {
        if($("div#width_of_facing > span#width_of_facing_err_msg").length == 0)
        {
            $("div#width_of_facing").append("<span id='width_of_facing_err_msg' style='color:red;'>Enter Width</span>");
        }
        $('html, body').animate({
            scrollTop: $("div#width_of_facing").offset().top
        }, 2000);
        return false;
    }
    else
    {
        if($("div#width_of_facing > span#width_of_facing_err_msg").length > 0)
        {
            $("div#width_of_facing > span#width_of_facing_err_msg").remove();
        }
        width_of_facing_valid = true;
    }

    if (plot_price_det != '') {
        plot_price_valid = true;
    }

    if (!plot_price_valid )  
    {
        if($("div#plot_price > span#plot_price_err_msg").length == 0)
        {
            $("div#plot_price").append("<span id='plot_price_err_msg' style='color:red;'>Enter Plot Price</span>");
        }
        $('html, body').animate({
            scrollTop: $("div#plot_price").offset().top
        }, 2000);
        return false;
    }
    else
    {
        if($("div#plot_price > span#plot_price_err_msg").length > 0)
        {
            $("div#plot_price > span#plot_price_err_msg").remove();
        }
        plot_price_valid = true;
    }


    if (price_per_sq_feet_det != '') {
        price_per_sq_feet_valid = true;
    }

    if (!price_per_sq_feet_valid )  
    {
        if($("div#price_per_sq_feet > span#price_per_sq_feet_err_msg").length == 0)
        {
            $("div#price_per_sq_feet").append("<span id='price_per_sq_feet_err_msg' style='color:red;'>Enter Price Per Sq feet</span>");
        }
        $('html, body').animate({
            scrollTop: $("div#price_per_sq_feet").offset().top
        }, 2000);
        return false;
    }
    else
    {
        if($("div#price_per_sq_feet > span#price_per_sq_feet_err_msg").length > 0)
        {
            $("div#price_per_sq_feet > span#price_per_sq_feet_err_msg").remove();
        }
        price_per_sq_feet_valid = true;
    }

    if ( !plot_area_measure_valid  || !plot_area_valid || !width_of_facing_valid || !plot_price_valid 
        || !price_per_sq_feet_valid )  
    {

        return false;
    }
    
    $("li#amenities > a").click();
}