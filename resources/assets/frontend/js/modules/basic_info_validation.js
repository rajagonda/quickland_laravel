
function basic_info_validation(e){
    e.preventDefault();
    var user_type = document.getElementsByName("user_type");
    var about_you = document.getElementsByName("about_you");
    var use_type = document.getElementsByName("use_type");
    var property_type = document.getElementsByName("property_type");
    var avail = document.getElementsByName("available_for");
    var poss = document.getElementsByName("possession");
    var poss_valid = avail_valid = user_type_valid = about_you_valid = use_type_valid = property_type_valid = false;

    var about_you_det = $(about_you).val();
    var about_you_valid = false;

    var i = 0;
    while (!user_type_valid && i < user_type.length) {
        if (user_type[i].checked) user_type_valid = true;
        i++;        
    }
    if (!user_type_valid )  
    {
        if($("div#you_r > span#you_r_err_msg").length == 0)
        {
            $("div#you_r").append("<span id='you_r_err_msg' style='color:red;'>Choose the option</span>");
        }
        $('html, body').animate({
            scrollTop: $("div#you_r").offset().top
        }, 2000);
        return false;
    }
    else
    {
        if($("div#you_r > span#you_r_err_msg").length > 0)
        {
            $("div#you_r > span#you_r_err_msg").remove();
        }
        user_type_valid = true;
    }

    if (about_you_det != '') {
        about_you_valid = true;
    }

    if (!about_you_valid )  
    {
        if($("div#about_you > span#about_you_err_msg").length == 0)
        {
            $("div#about_you").append("<span id='about_you_err_msg' style='color:red;'>Required</span>");
        }
        $('html, body').animate({
            scrollTop: $("div#about_you").offset().top
        }, 2000);
        return false;
    }
    else
    {
        if($("div#about_you > span#about_you_err_msg").length > 0)
        {
            $("div#about_you > span#about_you_err_msg").remove();
        }
        about_you_valid = true;
    }

    var i = 0;
    while (!user_type_valid && i < user_type.length) {
        if (user_type[i].checked) user_type_valid = true;
        i++;        
    }
    if (!user_type_valid )  
    {
        if($("div#you_r > span#you_r_err_msg").length == 0)
        {
            $("div#you_r").append("<span id='you_r_err_msg' style='color:red;'>Choose the option</span>");
        }
        $('html, body').animate({
            scrollTop: $("div#you_r").offset().top
        }, 2000);
        return false;
    }
    else
    {
        if($("div#you_r > span#you_r_err_msg").length > 0)
        {
            $("div#you_r > span#you_r_err_msg").remove();
        }
        user_type_valid = true;
    }

    i = 0;
    while (!use_type_valid && i < use_type.length) {
        if (use_type[i].checked) use_type_valid = true;
        i++;        
    }

    if (!use_type_valid )  
    {
        if($("div#i_want > span#i_want_err_msg").length == 0)
        {
            $("div#i_want").append("<span id='i_want_err_msg' style='color:red;'>Choose the option</span>");
        }

        $('html, body').animate({
            scrollTop: $("div#i_want").offset().top
        }, 2000);
        return false;
    }
    else
    {
        if($("div#i_want > span#i_want_err_msg").length > 0)
        {
            $("div#i_want > span#i_want_err_msg").remove();
        }
        use_type_valid = true;
    }

    i = 0;
    while (!property_type_valid && i < property_type.length) {
        if (property_type[i].checked) property_type_valid = true;
        i++;        
    }

    if (!property_type_valid )  
    {
        if($("div#prop_type > span#prop_type_err_msg").length == 0)
        {
            $("div#prop_type").append("<span id='prop_type_err_msg' style='color:red;'>Choose the option</span>");
        }

        $('html, body').animate({
            scrollTop: $("div#prop_type").offset().top
        }, 2000);
        return false;
    }
    else
    {
        if($("div#prop_type > span#prop_type_err_msg").length > 0)
        {
            $("div#prop_type > span#prop_type_err_msg").remove();
        }
        property_type_valid = true;
    }

    i = 0;
    while (!avail_valid && i < avail.length) {
        if (avail[i].checked) avail_valid = true;
        i++;        
    }

    if (!avail_valid )  
    {
        if($("div#avail > span#avail_err_msg").length == 0)
        {
            $("div#avail").append("<span id='avail_err_msg' style='color:red;'>Choose the option</span>");
        }

        $('html, body').animate({
            scrollTop: $("div#avail").offset().top
        }, 2000);
        return false;
    }
    else
    {
        if($("div#avail > span#avail_err_msg").length > 0)
        {
            $("div#avail > span#avail_err_msg").remove();
        }
        avail_valid = true;
    }

    i = 0;
    while (!poss_valid && i < poss.length) {
        if (poss[i].checked) poss_valid = true;
        i++;        
    }

    if (!poss_valid )  
    {
        if($("div#poss > span#poss_err_msg").length == 0)
        {
            $("div#poss").append("<span id='poss_err_msg' style='color:red;'>Choose the option</span>");
        }

        $('html, body').animate({
            scrollTop: $("div#poss").offset().top
        }, 2000);

        return false;
    }
    else
    {
        if($("div#poss > span#poss_err_msg").length > 0)
        {
            $("div#poss > span#poss_err_msg").remove();
        }
        poss_valid = true;
    }


    if (!poss_valid || !user_type_valid || !about_you_valid || !use_type_valid || !property_type_valid || !avail_valid)  
    {

        return false;
    }

    $("li#location-item > a").click();
}