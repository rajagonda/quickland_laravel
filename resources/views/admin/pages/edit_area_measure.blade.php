@extends('admin.layout')
@section('content')

@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<div>
  <div class="row">
                <div class="col-sm-6">
                    <h2>Edit Measure </h2>
                </div>
                
            </div>
            <form method="post" action="{{ url('/area/measure/update') }}/<?= $measure_det['plot_measure_id'] ?>" enctype="multipart/form-data">

  @csrf
  
  <div class="form-group">
    
    <input type="text" class="form-control" id="plot_measure" name="plot_measure" value="<?= $measure_det['plot_measure'] ?>" placeholder="Property Type">
  </div>

<div class="form-group text-center">
  
 <input type="submit" name="edit" class="btn btn-primary input-lg" value="Update" />
</div>
</form>
</div>
@endsection