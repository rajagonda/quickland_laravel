@extends('admin.layout')
@section('content')
@if($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div>
<div class="row">
                <div class="col-sm-6">
                    <h2>Add Area Measure </h2>
                </div>
                
            </div>
<form method="post" action="{{ url('/area/measure/store') }}" enctype="multipart/form-data">

    @csrf
    <div class="form-group">
    
    <input type="text" class="form-control" id="plot_measure" name="plot_measure" placeholder="Plot Measure">
  </div>
  
  <button type="submit" class="btn btn-default">Submit</button>

</form>
</div>
@endsection