@extends('admin.layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{url('/public/css/grid.css')}}">

<div>
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-8">
                    <h2>Possessions</h2>
                </div>
                <div class="col-sm-4">
                    <button type="button" class="btn btn-info add-new"><a href="{{url('property/possession/add')}}"><i class="fa fa-plus"></i> Add New</a></button>
                </div>
            </div>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th> Id</th>
                    <th> Possession</th>

                    <th> Created</th>
                    <th> Updated </th>
                    <th> Actions </th>
                </tr>
            </thead>
            <tbody>
                @foreach($possessions as $possession)
                <tr>
                    <td> {{ $possession['possession_id'] }} </td>
                    <td> {{ $possession['possession'] }} </td>
                    <td> {{ $possession['created_at'] }} </td>
                    <td> {{ $possession['updated_at'] }} </td>
                    <td>

                        <a href="{{url('/property/possession/edit')}}/{{ $possession['possession_id'] }}" class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                        <a href="javascript:void(0);" class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                    </td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("a.delete").on('click',function(e){
        
            if (!confirm("Do you want to delete")){
              return false;
          }
          else
          {
            window.location.href = "{{url('/property/possession/delete')}}/<?= $possession['possession_id'] ?>";
          }
      });
    });
</script>
@stop
