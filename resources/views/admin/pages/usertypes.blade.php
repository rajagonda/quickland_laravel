@extends('admin.layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{url('/public/css/grid.css')}}">

<div>
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-8">
                    <h2>User Types </h2>
                </div>
                <div class="col-sm-4">
                    <button type="button" class="btn btn-info add-new"><a href="{{url('/usertype/add')}}"><i class="fa fa-plus"></i> Add New</a></button>
                </div>
            </div>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th> Id</th>
                    <th> User Type</th>

                    <th> Created</th>
                    <th> Updated </th>
                    <th> Actions </th>
                </tr>
            </thead>
            <tbody>
                @foreach($user_types as $user_type)
                <tr>
                    <td> {{ $user_type['user_type_id'] }} </td>
                    <td> {{ $user_type['user_type'] }} </td>
                    <td> {{ $user_type['created_at'] }} </td>
                    <td> {{ $user_type['updated_at'] }} </td>
                    <td>

                        <a href="{{url('/usertype/edit')}}/{{ $user_type['user_type_id'] }}" class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                        <a href="javascript:void(0);" class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                    </td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("a.delete").on('click',function(e){
        
            if (!confirm("Do you want to delete")){
              return false;
          }
          else
          {
            window.location.href = "{{url('/usertype/delete')}}/<?= $user_type['user_type_id'] ?>";
          }
      });
    });
</script>
@stop
