@extends('admin.layout')
@section('content')
@if($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div>
<div class="row">
                <div class="col-sm-6">
                    <h2>Add User Type </h2>
                </div>
                
            </div>
<form method="post" action="{{ url('/usertype/store') }}" enctype="multipart/form-data">

    @csrf
    <div class="form-group">
    
    <input type="text" class="form-control" id="user_type" name="user_type" placeholder="User Type">
  </div>
  
  <button type="submit" class="btn btn-default">Submit</button>

</form>
</div>
@endsection