@extends('admin.layout')
@section('content')
@if($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div>
<div class="row">
                <div class="col-sm-6">
                    <h2>Add Possession </h2>
                </div>
                
            </div>
<form method="post" action="{{ url('/property/possession/store') }}" enctype="multipart/form-data">

    @csrf
    <div class="form-group">
    
    <input type="text" class="form-control" id="possession" name="possession" placeholder="Possession">
  </div>
  
  <button type="submit" class="btn btn-default">Submit</button>

</form>
</div>
@endsection