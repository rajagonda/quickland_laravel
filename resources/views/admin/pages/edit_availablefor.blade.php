@extends('admin.layout')
@section('content')

@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<div>
  <div class="row">
                <div class="col-sm-6">
                    <h2>Edit Available for </h2>
                </div>
                
            </div>
            <form method="post" action="{{ url('/property/availability/update') }}/<?= $availablefor_det['availability_id'] ?>" enctype="multipart/form-data">

  @csrf
  
  <div class="form-group">
    
    <input type="text" class="form-control" id="available_for" name="available_for" value="<?= $availablefor_det['available_for'] ?>" placeholder="Available For">
  </div>

<div class="form-group text-center">
  
 <input type="submit" name="edit" class="btn btn-primary input-lg" value="Update" />
</div>
</form>
</div>
@endsection