<!-- sidebar nav -->
<ul class="nav nav-pills nav-stacked">
    <li><a href="{{ url('/') }}/admin/usertypes">User Types</a></li>
    <li><a href="{{ url('/') }}/property/types">Property Types</a></li>
    <li><a href="{{ url('/') }}/property/uses">Property Uses</a></li>
    <li><a href="{{ url('/') }}/property/availability">Availability</a></li>
    <li><a href="{{ url('/') }}/property/possessions">Possessions</a></li>
    <li><a href="{{ url('/') }}/area/measures">Area Measures</a></li>
    
</ul>