@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">


                    <div class="login-wrapper">
                        <div class="ui-dash tab-content">

                            <div class="container tab-pane active " id="register" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-8">
                                        <form id="register-form" action="#" method="post">
                                            @csrf


                                            <div id="reg-status">

                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="user_name" id="user_name"
                                                       tabindex="1" class="form-control"
                                                       placeholder="Your Name" value="">
                                            </div>
                                            <div class="form-group">
                                                <input type="email" name="email" id="email" tabindex="1"
                                                       class="form-control" placeholder="Email Address"
                                                       value="">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="user_password"
                                                       id="user_password" tabindex="2" class="form-control"
                                                       placeholder="Password">
                                            </div>

                                            <div class="form-group">
                                                <input type="text" name="phone" id="phone" tabindex="1"
                                                       class="form-control" placeholder="Your Phone Number"
                                                       value="">
                                            </div>
                                            <div class="res-box text-left">
                                                <input type="checkbox" tabindex="3" class="" name="remember"
                                                       id="remember">
                                                <label for="remember">I've read and accept terms &amp;
                                                    conditions</label>
                                            </div>
                                            <div class="res-box text-center mt-30">
                                                <button id="signup" type="submit" class="btn v8"><i
                                                            class="lnr lnr-enter"></i>Sign Up
                                                </button>
                                            </div>
                                        </form>
                                        <div class="social-profile-login  text-left mt-20">
                                            <h5>or Sign Up with</h5>
                                            <ul class="social-btn">
                                                <li class="bg-fb"><a href="#"><i
                                                                class="fab fa-facebook-f"></i></a></li>
                                                <li class="bg-tt"><a href="#"><i class="fab fa-twitter"></i></a>
                                                </li>
                                                <li class="bg-ig"><a href="#"><i
                                                                class="fab fa-instagram"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="signup-wrapper">
                                            <img src="{{ asset('/frontend/images/others/login-1.png') }}"
                                                 alt="...">
                                            <p>Create an account to find the best Property for you.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane " id="otp" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form id="otp-form" action="#" method="post">
                                            @csrf

                                            <input type="hidden" value="" name="user_id"
                                                   class="otp_userid"/>

                                            <div id="otp-status">

                                            </div>


                                            <div class="form-group">
                                                <div class="signup-wrapper">
                                                                <span class="fa fa-mail-bulk"
                                                                      style="font-size: 50px"></span>
                                                    <small>Create an account to find the best Property for
                                                        you.</small>

                                                    <p>email: <span class="otp_email"></span> <a href="">Edit</a>
                                                    </p>
                                                </div>
                                            </div>


                                            <div class="form-group">

                                                <input type="text" name="otp" id="otp"
                                                       tabindex="1" class="form-control"
                                                       placeholder="Enter Otp" value="">
                                            </div>


                                            <div class="form-group">
                                                <div class="signup-wrapper">

                                                    <p>dont recive otp ? <a href="">resend email</a></p>
                                                </div>
                                            </div>


                                            <div class="res-box text-center mt-30">
                                                <button id="signup" type="submit" class="btn v8">
                                                    <i class="lnr lnr-enter"></i>
                                                    Varify OTP & Process
                                                </button>
                                            </div>
                                        </form>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>



{{--                    <form method="POST" action="{{ route('register') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>--}}

{{--                                @error('name')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Register') }}--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
