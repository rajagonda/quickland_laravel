<!doctype html>
<html>

<head>
    @include('frontend.includes.head')

    <link href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
    @yield('head')

</head>

<body>
<div class="page-wrapper">


    @include('frontend.includes.header')


    @yield('content')

    <div id="app">

    </div>


</div>


@include('frontend.includes.footer')


<script src="{{ asset('/frontend/js/site.js') }}"></script>


<script src="https://code.jquery.com/jquery-migrate-3.1.0.js"></script>


<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous"></script>


<script src="{{ asset('/js/customScript.js') }}"></script>

<script>



    $(function () {
        // datepickeUi();
    })
</script>

@if ( \Illuminate\Support\Facades\Auth::check())
@else

    @include('frontend.includes.login-register')
    @include('frontend.includes.login_register_script')
@endif



@yield('footer_script')


</body>

</html>