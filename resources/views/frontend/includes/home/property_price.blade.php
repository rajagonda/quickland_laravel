<!--Property Price section starts-->
        <div class="category-section bg-xs bg-fixed" style="background-image: url(public/images/bg/category-bg-3.jpg)">
            <div class="overlay op-5"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="category-content text-center">
                            <h3>Looking to Buy a new property or Sell an existing one? </h3>
                            <a href="#" class="btn v3">Rent Properties</a>
                            <a href="#" class="btn v6">Buy Properties</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Property Price section ends-->