<!--Trending events starts-->
        <div class="trending-places mt-135">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title v1">
                            <p>Find rental properties anywhere</p>
                            <h2>Discover Popular Properties</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 col-sm-12">
                        <div class="single-property-box">
                            <div class="property-item">
                                <a class="property-img" href="single-listing-two.html"><img src="{{ asset('/frontend/images/property/property_3.jpg') }}" alt="#"></a>
                                <ul class="feature_text">
                                    <li class="feature_or"><span>For Rent</span></li>
                                </ul>
                                <div class="property-author-wrap">
                                    <a href="#" class="property-author">
                                        <img src="{{ asset('/frontend/images/agents/agency_2.jpg') }}" alt="...">
                                        <span>Zilion Properties</span>
                                    </a>
                                    <ul class="save-btn">
                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="property-title-box">
                                <h4><a href="single-listing-one.html">Family home in Glasgow</a></h4>
                                <div class="property-location">
                                    <i class="fa fa-map-marker-alt"></i>
                                    <p>60 High St, Glasgow, London</p>
                                </div>
                                <ul class="property-feature">
                                    <li> <i class="fas fa-bed"></i>
                                        <span>3 Bedrooms</span>
                                    </li>
                                    <li> <i class="fas fa-bath"></i>
                                        <span>3 Bath</span>
                                    </li>
                                    <li> <i class="fas fa-arrows-alt"></i>
                                        <span>1982 sq ft</span>
                                    </li>
                                    <li> <i class="fas fa-car"></i>
                                        <span>1 Garage</span>
                                    </li>
                                </ul>
                                <div class="trending-bottom">
                                    <div class="trend-left float-left">
                                        <ul class="product-rating">
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                        </ul>
                                    </div>
                                    <a class="trend-right float-right">
                                        <div class="trend-open">
                                            <p>$7500<span class="per_month">month</span> </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 col-sm-12">
                        <div class="single-property-box">
                            <div class="property-item">
                                <a class="property-img" href="single-listing-two.html"><img src="{{ asset('/frontend/images/property/property_6.jpg') }}" alt="#"></a>
                                <ul class="feature_text">
                                    <li class="feature_cb"><span> New</span></li>
                                    <li class="feature_or"><span>For Sale</span></li>
                                </ul>
                                <div class="property-author-wrap">
                                    <a href="#" class="property-author">
                                        <img src="{{ asset('/frontend/images/agents/agent_min_2.jpg') }}" alt="...">
                                        <span>Bob Haris</span>
                                    </a>
                                    <ul class="save-btn">
                                        <li data-toggle="tooltip" data-placement="top" title="Video"><a href="https://www.youtube.com/watch?v=v_ATnE02qFs" class="property-yt"><i class="fas fa-play"></i></a></li>
                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="property-title-box">
                                <h4><a href="single-listing-one.html">Apartment in Cecil Lake</a></h4>
                                <div class="property-location">
                                    <i class="fa fa-map-marker-alt"></i>
                                    <p>131 midlas , Cecil Lake, BC</p>
                                </div>
                                <ul class="property-feature">
                                    <li> <i class="fas fa-bed"></i>
                                        <span>3 Bedrooms</span>
                                    </li>
                                    <li> <i class="fas fa-bath"></i>
                                        <span>2 Bath</span>
                                    </li>
                                    <li> <i class="fas fa-arrows-alt"></i>
                                        <span>1600 sq ft</span>
                                    </li>
                                    <li> <i class="fas fa-car"></i>
                                        <span>1 Garage</span>
                                    </li>
                                </ul>
                                <div class="trending-bottom">
                                    <div class="trend-left float-left">
                                        <ul class="product-rating">
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                        </ul>
                                    </div>
                                    <a class="trend-right float-right">
                                        <div class="trend-open">
                                            <p><span class="per_sale">starts from</span>$9000</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 col-sm-12">
                        <div class="single-property-box">
                            <div class="property-item">
                                <a class="property-img" href="single-listing-two.html"><img src="{{ asset('/frontend/images/property/property_5.jpg') }}" alt="#">
                                </a>
                                <ul class="feature_text">
                                    <li class="feature_or"><span>For Sale</span></li>
                                </ul>
                                <div class="property-author-wrap">
                                    <a href="#" class="property-author">
                                        <img src="{{ asset('/frontend/images/agents/agency_3.jpg') }}" alt="...">
                                        <span>Seaside Properties</span>
                                    </a>
                                    <ul class="save-btn">
                                        <li data-toggle="tooltip" data-placement="top" title="Photos"><a href=".photo-gallery" class="btn-gallery"><i class="lnr lnr-camera"></i></a></li>
                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
                                    </ul>
                                    <div class="hidden photo-gallery">
                                        <a href="{{ asset('/frontend/images/single-listing/property_view_1.jpg') }}"></a>
                                        <a href="{{ asset('/frontend/images/single-listing/property_view_2.jpg') }}"></a>
                                        <a href="{{ asset('/frontend/images/single-listing/property_view_3.jpg') }}"></a>
                                        <a href="{{ asset('/frontend/images/single-listing/property_view_4.jpg') }}"></a>
                                        <a href="{{ asset('/frontend/images/single-listing/property_view_5.jpg') }}"></a>
                                        <a href="{{ asset('/frontend/images/single-listing/property_view_6.jpg') }}"></a>
                                        <a href="{{ asset('/frontend/images/single-listing/property_view_7.jpg') }}"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="property-title-box">
                                <h4><a href="single-listing-one.html">Luxury Villa in Birmingham</a></h4>
                                <div class="property-location">
                                    <i class="fa fa-map-marker-alt"></i>
                                    <p>159 Dudley Rd, Birmingham, UK</p>
                                </div>
                                <ul class="property-feature">
                                    <li> <i class="fas fa-bed"></i>
                                        <span>5 Bedrooms</span>
                                    </li>
                                    <li> <i class="fas fa-bath"></i>
                                        <span>4 Bath</span>
                                    </li>
                                    <li> <i class="fas fa-arrows-alt"></i>
                                        <span>3000 sq ft</span>
                                    </li>
                                    <li> <i class="fas fa-car"></i>
                                        <span>2 Garage</span>
                                    </li>
                                </ul>
                                <div class="trending-bottom">
                                    <div class="trend-left float-left">
                                        <ul class="product-rating">
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                        </ul>
                                    </div>
                                    <a class="trend-right float-right">
                                        <div class="trend-open">
                                            <p><span class="per_sale">starts from</span>$21000</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 col-sm-12">
                        <div class="single-property-box">
                            <div class="property-item">
                                <a class="property-img" href="single-listing-two.html"><img src="{{ asset('/frontend/images/property/property_6.jpg') }}" alt="#"></a>
                                <ul class="feature_text">
                                    <li class="feature_cb"><span> New</span></li>
                                    <li class="feature_or"><span>For Sale</span></li>
                                </ul>
                                <div class="property-author-wrap">
                                    <a href="#" class="property-author">
                                        <img src="{{ asset('/frontend/images/agents/agent_min_2.jpg') }}" alt="...">
                                        <span>Bob Haris</span>
                                    </a>
                                    <ul class="save-btn">
                                        <li data-toggle="tooltip" data-placement="top" title="Video"><a href="https://www.youtube.com/watch?v=v_ATnE02qFs" class="property-yt"><i class="fas fa-play"></i></a></li>
                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="property-title-box">
                                <h4><a href="single-listing-one.html">Apartment in Cecil Lake</a></h4>
                                <div class="property-location">
                                    <i class="fa fa-map-marker-alt"></i>
                                    <p>131 midlas , Cecil Lake, BC</p>
                                </div>
                                <ul class="property-feature">
                                    <li> <i class="fas fa-bed"></i>
                                        <span>3 Bedrooms</span>
                                    </li>
                                    <li> <i class="fas fa-bath"></i>
                                        <span>2 Bath</span>
                                    </li>
                                    <li> <i class="fas fa-arrows-alt"></i>
                                        <span>1600 sq ft</span>
                                    </li>
                                    <li> <i class="fas fa-car"></i>
                                        <span>1 Garage</span>
                                    </li>
                                </ul>
                                <div class="trending-bottom">
                                    <div class="trend-left float-left">
                                        <ul class="product-rating">
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                        </ul>
                                    </div>
                                    <a class="trend-right float-right">
                                        <div class="trend-open">
                                            <p><span class="per_sale">starts from</span>$9000</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 col-sm-12">
                        <div class="single-property-box">
                            <div class="property-item">
                                <a class="property-img" href="single-listing-two.html"><img src="{{ asset('/frontend/images/property/property_9.jpg') }}" alt="#"></a>
                                <ul class="feature_text">
                                    <li class="feature_cb"><span> Featured</span></li>
                                    <li class="feature_or"><span>For Sale</span></li>
                                </ul>
                                <div class="property-author-wrap">
                                    <a href="#" class="property-author">
                                        <img src="{{ asset('/frontend/images/agents/agent_min_3.jpg') }}" alt="...">
                                        <span>Jim Carry</span>
                                    </a>
                                    <ul class="save-btn">
                                        <li data-toggle="tooltip" data-placement="top" title="Video"><a href="https://www.youtube.com/watch?v=v_ATnE02qFs" class="property-yt"><i class="fas fa-play"></i></a></li>
                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="property-title-box">
                                <h4><a href="single-listing-one.html">Luxury Condo in Mariwood</a></h4>
                                <div class="property-location">
                                    <i class="fa fa-map-marker-alt"></i>
                                    <p>164 Mariwood Rd , Campbell River, BC</p>
                                </div>
                                <ul class="property-feature">
                                    <li> <i class="fas fa-bed"></i>
                                        <span>6 Bedrooms</span>
                                    </li>
                                    <li> <i class="fas fa-bath"></i>
                                        <span>5 Bath</span>
                                    </li>
                                    <li> <i class="fas fa-arrows-alt"></i>
                                        <span>2400 sq ft</span>
                                    </li>
                                    <li> <i class="fas fa-car"></i>
                                        <span>2 Garage</span>
                                    </li>
                                </ul>
                                <div class="trending-bottom">
                                    <div class="trend-left float-left">
                                        <ul class="product-rating">
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                        </ul>
                                    </div>
                                    <a class="trend-right float-right">
                                        <div class="trend-open">
                                            <p><span class="per_sale">starts from</span>$75000</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 col-sm-12">
                        <div class="single-property-box">
                            <div class="property-item">
                                <a class="property-img" href="single-listing-two.html"><img src="{{ asset('/frontend/images/property/property_7.jpg') }}" alt="#"> </a>
                                <ul class="feature_text">
                                    <li class="feature_cb"><span> Featured</span></li>
                                    <li class="feature_or"><span>For Rent</span></li>
                                </ul>
                                <div class="property-author-wrap">
                                    <a href="#" class="property-author">
                                        <img src="{{ asset('/frontend/images/agents/agency_1.jpg') }}" alt="...">
                                        <span>Carmen Properties</span>
                                    </a>
                                    <ul class="save-btn">
                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="property-title-box">
                                <h4><a href="single-listing-one.html">Villa on Sunbury</a></h4>
                                <div class="property-location">
                                    <i class="fa fa-map-marker-alt"></i>
                                    <p>39 Casey Ave, Sunbury, VIC 3429</p>
                                </div>
                                <ul class="property-feature">
                                    <li> <i class="fas fa-bed"></i>
                                        <span>5 Bedrooms</span>
                                    </li>
                                    <li> <i class="fas fa-bath"></i>
                                        <span>4 Bath</span>
                                    </li>
                                    <li> <i class="fas fa-arrows-alt"></i>
                                        <span>2048 sq ft</span>
                                    </li>
                                    <li> <i class="fas fa-car"></i>
                                        <span>2 Garage</span>
                                    </li>
                                </ul>
                                <div class="trending-bottom">
                                    <div class="trend-left float-left">
                                        <ul class="product-rating">
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                        </ul>
                                    </div>
                                    <a class="trend-right float-right">
                                        <div class="trend-open">
                                            <p>$9200<span class="per_month">month</span> </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 text-center mt-1">
                        <a href="tab-fullwidth.html" class="btn v9">Browse More</a>
                    </div>
                </div>
            </div>
        </div>
        <!--Trending events ends-->