<!--Call to action starts-->
        <div class="call-to-action consult-form v2  bg-fixed bg-h mt-140" style="background-image: url(public/images/bg/call-to-action-bg.jpg)">
            <div class="overlay op-7"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="action-title sm-center">
                            <h2>Get a Free Consultation</h2>
                            <p>With no pressure to commit and no money collected until we sell your home, why not schedule your free consultation and let our expert knowledge and resources help you realize your goal of buying or selling a home.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 offset-lg-2">
                        <div class="row consult-form">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control filter-input mt-0" placeholder="Your name">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control filter-input mt-0" placeholder="Your mail">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control filter-input mt-0" placeholder="Your phone number">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="listing-input hero__form-input  custom-select">
                                        <option>Select your city</option>
                                        <option>NewYork</option>
                                        <option>Milan</option>
                                        <option>Florida</option>
                                        <option>Miami</option>
                                        <option>Havana</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="listing-input hero__form-input  custom-select">
                                        <option>Buy a property</option>
                                        <option>Rent a property</option>
                                        <option>Others </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <a href="#" class="btn v8">Send Message</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Call to action ends-->