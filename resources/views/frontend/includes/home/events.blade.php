
<div class="trending-places mt-30 pb-130">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title v1">
                    <p>Browse some of our</p>
                    <h2>Recent Properties</h2>
                </div>
            </div>
            <div class="swiper-container trending-place-wrap">
                <div class="swiper-wrapper">
                    @foreach($data as $prop)
                    <div class="swiper-slide">
                        <div class="single-property-box">
                            <div class="property-item">
                                @php
                                $images = explode(',',$prop->new_property_files);
                                $first_image_name = $images[0];
                                @endphp
                                <a class="property-img" href="{{ url('/property/show') }}/{{ $prop->property_id}}/2"><img src="{{ asset('/property_photos')}}/{{$prop->property_id}}/{{$first_image_name}}" alt="#"> </a>
                                <ul class="feature_text">
                                    <li class="feature_cb"><span>New</span></li>
                                    <li class="feature_or"><span>For Rent</span></li>
                                </ul>
                                <div class="property-author-wrap">
                                    <a href="#" class="property-author">
                                        <img src="{{ asset('/frontend/images/agents/agent_min_2.jpg') }}" alt="...">
                                        <span>Bob haris</span>
                                    </a>
                                    <ul class="save-btn">
                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="property-title-box">
                                <h4><a href="single-listing-one.html">{{ $prop->project_name }}</a></h4>
                                <div class="property-location">
                                    <i class="fa fa-map-marker-alt"></i>
                                    <p>{{ $prop->property_address }}</p>
                                </div>
                                <ul class="property-feature">
                                    <li> <i class="fas fa-bed"></i>
                                        <span>3 Bedrooms</span>
                                    </li>
                                    <li> <i class="fas fa-bath"></i>
                                        <span>2 Bath</span>
                                    </li>
                                    <li> <i class="fas fa-arrows-alt"></i>
                                        <span>{{ $prop->plot_area }} sq ft</span>
                                    </li>
                                    <li> <i class="fas fa-car"></i>
                                        <span>1 Garage</span>
                                    </li>
                                </ul>
                                <div class="trending-bottom">
                                    <div class="trend-left float-left">
                                        <ul class="product-rating">
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                        </ul>
                                    </div>
                                    <a class="trend-right float-right">
                                        <div class="trend-open">
                                            <p>Rs{{ $prop->plot_price }}<span class="per_month">month</span> </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endforeach




                </div>
            </div>
            <div class="trending-pagination"></div>
        </div>
    </div>
</div>
        