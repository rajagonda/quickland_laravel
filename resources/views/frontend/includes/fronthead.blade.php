<head>
    <!-- Metas -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="LionCoders" />
    <!-- Links -->
    <link rel="icon" type="image/png" href="#" />
    <!-- google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    <!-- Plugins CSS -->
    <link href="{{ asset('/css/plugin.css') }}" rel="stylesheet" />
    <!-- style CSS -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" />
    <!-- Document Title -->
    <title>QuickLands Post Your Property</title>
    <script src="{{ asset('/js/plugin.js') }}"></script>
    
    <!--Main js-->
    <script src="{{ asset('/js/main.js') }}"></script>
</head>