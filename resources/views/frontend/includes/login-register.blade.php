{{--<!-- Button to Open the Modal -->--}}
{{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">--}}
{{--    Open modal--}}
{{--</button>--}}

<!-- The Modal -->
{{--<div class="modal" id="user-login-popup">--}}
{{--    <div class="modal-dialog">--}}
{{--        <div class="modal-content">--}}

{{--            <!-- Modal Header -->--}}
{{--            <div class="modal-header">--}}
{{--                <h4 class="modal-title">Modal Heading</h4>--}}
{{--                <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--            </div>--}}

{{--            <!-- Modal body -->--}}
{{--            <div class="modal-body">--}}
{{--                Modal body..--}}
{{--            </div>--}}

{{--            <!-- Modal footer -->--}}
{{--            <div class="modal-footer">--}}
{{--                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}







<!--login Modal starts-->
<div class="modal " id="user-login-popup">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i
                                class="lnr lnr-cross"></i></span></button>
            </div>
            <div class="modal-body">
                <!--User Login section starts-->
                <div class="user-login-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">


                                {{--                                <ul class="nav nav-tabs">--}}
                                {{--                                    <li class="nav-item">--}}
                                {{--                                        <a class="nav-link active" data-toggle="tab" href="#home">Home</a>--}}
                                {{--                                    </li>--}}
                                {{--                                    <li class="nav-item">--}}
                                {{--                                        <a class="nav-link" data-toggle="tab" href="#menu1">Menu 1</a>--}}
                                {{--                                    </li>--}}
                                {{--                                    <li class="nav-item">--}}
                                {{--                                        <a class="nav-link" data-toggle="tab" href="#menu2">Menu 2</a>--}}
                                {{--                                    </li>--}}
                                {{--                                </ul>--}}

                                {{--                                <!-- Tab panes -->--}}
                                {{--                                <div class="tab-content">--}}
                                {{--                                    <div id="home" class="container tab-pane active"><br>--}}
                                {{--                                        <h3>HOME</h3>--}}
                                {{--                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
                                {{--                                    </div>--}}
                                {{--                                    <div id="menu1" class="container tab-pane fade"><br>--}}
                                {{--                                        <h3>Menu 1</h3>--}}
                                {{--                                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>--}}
                                {{--                                    </div>--}}
                                {{--                                    <div id="menu2" class="container tab-pane fade"><br>--}}
                                {{--                                        <h3>Menu 2</h3>--}}
                                {{--                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}


                                <ul class="ui-list nav nav-tabs mb-30" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#login" role="tab"
                                           aria-selected="true">Login</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#register" role="tab"
                                           aria-selected="false">Register</a>
                                    </li>
                                </ul>
                                <div class="login-wrapper">
                                    <div class="ui-dash tab-content">
                                        <div class="container tab-pane active" id="login" role="tabpanel">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <form id="login-form" action="#" method="post">
                                                        @csrf

                                                        <div id="login-status">

                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" name="email" id="email"
                                                                   tabindex="1" class="form-control" placeholder="email"
                                                                   required="">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="password" name="password" id="password"
                                                                   tabindex="2" class="form-control"
                                                                   placeholder="Password">
                                                        </div>
                                                        <div class="row mt-20">
                                                            <div class="col-md-6 col-12 text-left">
                                                                <div class="res-box">
                                                                    <input id="check-l" type="checkbox" name="check">
                                                                    <label for="check-l">Remember me</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12 text-right">
                                                                <div class="res-box sm-left">
                                                                    <a href="#" tabindex="5" class="forgot-password">Forgot
                                                                        Password?</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="res-box text-center mt-30">
                                                            <button id="signin" type="submit" class="btn v8"><span
                                                                        class="lnr lnr-exit"></span> Log In
                                                            </button>
                                                        </div>
                                                    </form>
                                                    <div class="social-profile-login  text-left mt-20">
                                                        <h5>or Login with</h5>
                                                        <ul class="social-btn">
                                                            <li class="bg-fb"><a href="#"><i
                                                                            class="fab fa-facebook-f"></i></a></li>
                                                            <li class="bg-tt"><a href="#"><i class="fab fa-twitter"></i></a>
                                                            </li>
                                                            <li class="bg-ig"><a href="#"><i
                                                                            class="fab fa-instagram"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="signup-wrapper">
                                                        <img src="{{ asset('/frontend/images/others/login-1.png') }}"
                                                             alt="...">
                                                        <p>Welcome Back! Please Login to your Account for latest
                                                            property listings.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container tab-pane " id="register" role="tabpanel">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <form id="register-form" action="#" method="post">
                                                        @csrf


                                                        <div id="reg-status">

                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" name="user_name" id="user_name"
                                                                   tabindex="1" class="form-control"
                                                                   placeholder="Your Name" value="">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="email" name="email" id="email" tabindex="1"
                                                                   class="form-control" placeholder="Email Address"
                                                                   value="">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="password" name="user_password"
                                                                   id="user_password" tabindex="2" class="form-control"
                                                                   placeholder="Password">
                                                        </div>

                                                        <div class="form-group">
                                                            <input type="text" name="phone" id="phone" tabindex="1"
                                                                   class="form-control" placeholder="Your Phone Number"
                                                                   value="">
                                                        </div>
                                                        <div class="res-box text-left">
                                                            <input type="checkbox" tabindex="3" class="" name="remember"
                                                                   id="remember">
                                                            <label for="remember">I've read and accept terms &amp;
                                                                conditions</label>
                                                        </div>
                                                        <div class="res-box text-center mt-30">
                                                            <button id="signup" type="submit" class="btn v8"><i
                                                                        class="lnr lnr-enter"></i>Sign Up
                                                            </button>
                                                        </div>
                                                    </form>
                                                    <div class="social-profile-login  text-left mt-20">
                                                        <h5>or Sign Up with</h5>
                                                        <ul class="social-btn">
                                                            <li class="bg-fb"><a href="#"><i
                                                                            class="fab fa-facebook-f"></i></a></li>
                                                            <li class="bg-tt"><a href="#"><i class="fab fa-twitter"></i></a>
                                                            </li>
                                                            <li class="bg-ig"><a href="#"><i
                                                                            class="fab fa-instagram"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="signup-wrapper">
                                                        <img src="{{ asset('/frontend/images/others/login-1.png') }}"
                                                             alt="...">
                                                        <p>Create an account to find the best Property for you.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane " id="otp" role="tabpanel">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form id="otp-form" action="#" method="post">
                                                        @csrf

                                                        <input type="hidden" value="" name="user_id"
                                                               class="otp_userid"/>

                                                        <div id="otp-status">

                                                        </div>


                                                        <div class="form-group">
                                                            <div class="signup-wrapper">
                                                                <span class="fa fa-mail-bulk"
                                                                      style="font-size: 50px"></span>
                                                                <small>Create an account to find the best Property for
                                                                    you.</small>

                                                                <p>email: <span class="otp_email"></span> <a href="">Edit</a>
                                                                </p>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">

                                                            <input type="text" name="otp" id="otp"
                                                                   tabindex="1" class="form-control"
                                                                   placeholder="Enter Otp" value="">
                                                        </div>


                                                        <div class="form-group">
                                                            <div class="signup-wrapper">

                                                                <p>dont recive otp ? <a href="">resend email</a></p>
                                                            </div>
                                                        </div>


                                                        <div class="res-box text-center mt-30">
                                                            <button id="signup" type="submit" class="btn v8">
                                                                <i class="lnr lnr-enter"></i>
                                                                Varify OTP & Process
                                                            </button>
                                                        </div>
                                                    </form>

                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--User login section ends-->
            </div>
        </div>
    </div>
</div>
<!--login Modal ends-->




