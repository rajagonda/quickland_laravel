<header class="header transparent scroll-hide">
    <!--Main Menu starts-->
    <div class="site-navbar-wrap v1">
        <div class="site-navbar">
            <div class="row align-items-center">
                <div class="col-lg-2 col-md-6 col-9 order-2 order-xl-1 order-lg-1 order-md-1">
                    <a class="navbar-brand" href="index.html">
                        <img src="{{ asset('/frontend/images/quicklands_logo.jpg') }}"
                             alt="logo" class="img-fluid">
                    </a>


                    <?php
                    //                    dump(\Illuminate\Support\Facades\Auth::user())
                    ?>
                </div>
                <div class="col-lg-6 col-md-1 col-3  order-3 order-xl-2 order-lg-2 order-md-3 pl-xs-0">
                    <nav class="site-navigation text-left">
                        <div class="container">
                            <ul class="site-menu js-clone-nav d-none d-lg-block">
                                <li class="has-children">
                                    <a href="#">Location</a>
                                    <ul class="dropdown">
                                        <li><a href="home-v1.html">Hyderabad</a></li>
                                        <li><a href="home-v2.html">Chennai</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ url('/property/list') }}">Property Listing</a></li>
                            </ul>

                        </div>
                    </nav>
                    <div class="d-lg-none sm-right">
                        <a href="#" class="mobile-bar js-menu-toggle">
                            <span class="lnr lnr-menu"></span>
                        </a>
                    </div>
                    <!--mobile-menu starts -->
                    <div class="site-mobile-menu">
                        <div class="site-mobile-menu-header">
                            <div class="site-mobile-menu-close  js-menu-toggle">
                                <span class="lnr lnr-cross"></span></div>
                        </div>
                        <div class="site-mobile-menu-body"></div>
                    </div>
                    <!--mobile-menu ends-->
                </div>
                <div class="col-lg-4 col-md-5 col-12 order-1 order-xl-3 order-lg-3 order-md-2 text-right pr-xs-0">
                    <div class="menu-btn">


                        @if ( \Illuminate\Support\Facades\Auth::check())
                            <div class="add-list float-right ml-3 mr-3">
                                <a class="btn v6" href="{{ route('site.user.logout_process') }}"><i
                                            class="lnr lnr-user"></i>Logout
                                </a>
                            </div>
                            <div class="add-list float-right ml-3 mr-3">
                                <a class="btn v6" href="{{ route('ajax.user.property.add') }}"><i
                                            class="lnr lnr-home"></i>Add
                                    Listing </a>
                            </div>
                        @else
                            <ul class="user-btn v2">
                                <li><a href="#" data-toggle="modal" data-target="#user-login-popup"><i
                                                class="lnr lnr-user"></i></a>
                                </li>

                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Main Menu ends-->
</header>