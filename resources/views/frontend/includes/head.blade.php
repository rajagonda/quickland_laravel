<head>
    <!-- Metas -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="LionCoders" />
    <!-- Links -->
    <link rel="icon" type="image/png" href="#" />

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

    <link href="{{ asset('/frontend/css/site_style.css') }}" rel="stylesheet" />

    <!-- Plugins CSS -->
{{--    <link href="{{ asset('/css/plugin.css') }}" rel="stylesheet" />--}}
{{--    <!-- style CSS -->--}}
{{--    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" />--}}
{{--    <!-- Document Title -->--}}
{{--    <title>Welcome to QuickLands</title>--}}
{{--    --}}
{{--    <script src="{{ asset('/js/plugin.js') }}"></script>--}}
{{--    <!--google maps-->--}}
{{--   --}}
{{--    <!--Main js-->--}}
{{--    <script src="{{ asset('/js/main.js') }}"></script>--}}



</head>