<script type="text/javascript">

    function userRegisterForm() {
        $('#register-form').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            rules: {
                user_name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true
                },
                user_password: {
                    required: true,
                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 10,
                }
            },
            messages: {
                user_name: {
                    // required: 'Enter email',
                    email: 'Enter a Name'
                },
                email: {
                    required: 'Enter email',
                    email: 'Enter a valid email'
                },
                user_password: {
                    required: 'Enter Password',
                },
                phone: {
                    required: 'Enter Phone',
                    number: "Enter Numbers Only",
                    minlength: "enter Numbers {0} Only",
                    maxlength: "enter Numbers {0} Only",
                }

            },
            submitHandler: function (form) {

                var postdata = $(form).serialize();

                $.ajax({
                    type: 'POST',
                    url: "{{ route('site.ajax.user.rigister') }}",
                    data: postdata,
                    success: function (res) {
                        console.log(res)

                        // var post_res = JSON.parse(res);

                        if (res.status) {
                            $("div#reg-status").html("");
                            $("div#reg-status").html("<h4 style='color:red;text-align:center;'>" + res.message + "</h4>");
                            $(".otp_email").html(res.user.email);

                            $(".otp_userid").val(res.user.id);


                            $("div#reg-status").html("");
                            $("div#register").slideUp();
                            $("div#otp").slideDown();


                            userRegisterFormOtpVarify();

                        } else {
                            $("div#reg-status").html("");
                            $("div#reg-status").html("<h4 style='color:red;text-align:center;'>" + res.message + "</h4>");
                            // $("div#reg-status").slideDown(function () {
                            //     setTimeout(function () {
                            //         $("div#reg-status").slideUp();
                            //     }, 5000);
                            // });
                        }
                    }
                });


                return false; // required to block normal submit since you used ajax
            }
        });
    }


    function userRegisterFormOtpVarify() {


        $('#otp-form').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            rules: {

                otp: {
                    required: true,
                    number: true,
                    minlength: 6,
                    maxlength: 6,
                }
            },
            messages: {

                otp: {
                    required: 'Enter OTP',
                    number: "Enter Numbers Only",
                    minlength: "enter Numbers {0} Only",
                    maxlength: "enter Numbers {0} Only",
                }

            },
            submitHandler: function (form) {

                var postdata = $(form).serialize();

                $.ajax({
                    type: 'POST',
                    url: "{{ route('site.ajax.user.rigister.otp_varify') }}",
                    data: postdata,
                    success: function (res) {
                        console.log(res)

                        if (res.status) {
                            window.location.href = "{{ route('site.home') }}";
                            // console.log("{{ url('/') }}");
                        } else {
                            $("div#otp-status").html("");
                            $("div#otp-status").html("<h4 style='color:red;text-align:center;'>" + res.message + "</h4>");


                        }


                    }
                });


                return false; // required to block normal submit since you used ajax
            }
        });


    }


    function userLogin_Ajax() {


        $('#login-form').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            rules: {

                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                },

            },
            messages: {

                email: {
                    required: 'Enter email',
                    email: 'Enter a valid email'
                },
                password: {
                    required: 'Enter Password',
                },


            },
            submitHandler: function (form) {

                var postdata = $(form).serialize();

                $.ajax({
                    type: 'POST',
                    url: "{{ route('site.ajax.user.login') }}",
                    data: postdata,
                    success: function (res) {
                        console.log(res)

                        // var post_res = JSON.parse(res);

                        if (res.status) {
                            $("div#login-status").html("");
                            $("div#login-status").html("<h4 style='color:red;text-align:center;'>" + res.message + "</h4>");

                            window.location.href = "{{ route('site.home') }}";
                        } else {
                            $("div#login-status").html("");
                            $("div#login-status").html("<h4 style='color:red;text-align:center;'>" + res.message + "</h4>");

                        }
                    }
                });


                return false; // required to block normal submit since you used ajax
            }
        });


    }

    $(document).ready(function (e) {


        userRegisterForm();

        userLogin_Ajax();

    })
</script>