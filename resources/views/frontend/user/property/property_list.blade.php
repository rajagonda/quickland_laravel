@extends('layouts.layout')
@section('head')



@endsection
@section('content')

    <div class="filter-wrapper style1">
        <div class="container bg-add-listing">
            <div class="row">
                <div class="col-xl-3 col-lg-12">

                    @include('frontend.user.left_menu')

                </div>
                <!--Sidebar ends-->
                <div class="col-xl-9 col-lg-12">


                    <div class="sidebar-content-right py-100">

                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif

{{--                        <div class="row pb-30 align-items-center">--}}
{{--                            <div class="col-lg-4 col-sm-7 col-7">--}}
{{--                                <div class="nice-select hero__form-input custom-select filter-sorting" tabindex="0">--}}
{{--                                    <span class="current">Sort by Newest</span>--}}
{{--                                    <ul class="list">--}}
{{--                                        <li class="option focus">Sort by Newest</li>--}}
{{--                                        <li class="option focus">Sort by Oldest</li>--}}
{{--                                        <li class="option focus">Sort by Featured</li>--}}
{{--                                        <li class="option focus">Sort by Price(Low to High)</li>--}}
{{--                                        <li class="option focus">Sort by Price(Low to High)</li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-lg-5 col-sm-12">--}}
{{--                                <div class="item-element res-box  text-right sm-left">--}}
{{--                                    <p>Showing <span>{{$data->firstItem()}}-{{$data->firstItem() + ($data->count() -1) }} of {{$data->total()}}</span>--}}
{{--                                        Listings</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}


                        <div class="item-wrapper pt-20    ">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade  show active  property-list" id="list-view">
                                    @if(!empty($data) && $data->count())
                                        @foreach($data as $key => $value)
                                            <div class="single-property-box">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-12">
                                                        <div class="property-item">
                                                            <a class="property-img"
                                                               href="{{ url('/property/show') }}/{{ $value->property_id}}/2">
                                                                @php
                                                                    $images = explode(',',$value->new_property_files);
                                                                    $first_image_name = $images[0];
                                                                @endphp
                                                                <img src="{{ asset('/property_photos')}}/{{$value->property_id}}/{{$first_image_name}}"
                                                                     alt="#">
                                                            </a>
                                                            <ul class="feature_text">
                                                                <li class="feature_cb"><span> Featured</span></li>
                                                                <li class="feature_or"><span>For Sale</span></li>
                                                            </ul>
                                                            <div class="property-author-wrap">
                                                                <a href="#" class="property-author">
                                                                    <img src="{{ asset('/frontend/images/agents/agent_min_1.jpg') }}"
                                                                         alt="...">
                                                                    <span>Agent Name</span>
                                                                </a>
                                                                <ul class="save-btn">
                                                                    <li data-toggle="tooltip" data-placement="top"
                                                                        title="Photos"><a href=".photo-gallery{{$key}}"
                                                                                          class="btn-gallery"><i
                                                                                    class="lnr lnr-camera"></i></a></li>
                                                                    <li data-toggle="tooltip" data-placement="top"
                                                                        title="" data-original-title="Bookmark"><a
                                                                                href="#"><i
                                                                                    class="lnr lnr-heart"></i></a></li>
                                                                </ul>
                                                                <div class="hidden photo-gallery{{$key}}">
                                                                    @foreach($images as $img)
                                                                        <a href="{{ asset('/property_photos')}}/{{$value->property_id}}/{{$img}}"></a>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 col-sm-12">
                                                        <div class="property-title-box">
                                                            <h4>
                                                                <a href="{{ url('/property/show') }}/{{ $value->property_id}}/1">{{$value->project_name}}</a>
                                                            </h4>
                                                            <div class="property-location">
                                                                <i class="fa fa-map-marker-alt"></i>
                                                                <p>{{$value->locality}}</p>
                                                            </div>
                                                            <ul class="property-feature">
                                                                <li><span class="trend-open"><p>₹{{$value->plot_price}} Lacs</p></span>
                                                                </li>
                                                                <li><span>{{$value->plot_area}} sq ft</span></li>
                                                            </ul>
                                                            <div class="property-desc">
                                                                <p>{{$value->about_property}} <label class="trend-open">...more</label>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="property-partition"></div>
                                                    <div class="col-md-9 col-sm-12">
                                                        <div class="property-btm-info">
                                                            <span>Posted 3 days ago by Owner</span>
                                                            <span>Green City</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12">
                                                        <div class="property-contact">
                                                            <button class="btn v8" type="submit">Contact Owner</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach
                                @endif

                                <!--pagination starts-->
                                    <div class="post-nav nav-res pt-50  ">
                                        <div class="row">
                                            <div class="col-md-8 offset-md-2  col-xs-12 ">
                                                <div class="page-num text-center">
                                                    @if ($data->lastPage() > 1)
                                                        <ul class="">
                                                            <li class="{{ ($data->currentPage() == 1) ? ' disabled' : '' }}">
                                                                <a href="{{ $data->url(1) }}"><i
                                                                            class="lnr lnr-chevron-left"></i></a>
                                                            </li>
                                                            @for ($i = 1; $i <= $data->lastPage(); $i++)
                                                                <li class="{{ ($data->currentPage() == $i) ? ' active' : '' }}">
                                                                    <a href="{{ $data->url($i) }}">{{ $i }}</a>
                                                                </li>
                                                            @endfor
                                                            <li class="{{ ($data->currentPage() == $data->lastPage()) ? ' disabled' : '' }}">
                                                                <a href="{{ $data->url($data->currentPage()+1) }}"><i
                                                                            class="lnr lnr-chevron-right"></i></a>
                                                            </li>
                                                        </ul>
                                                @endif
                                                <!-- <ul>
                                                        <li class="active"><a href="#">1</a></li>
                                                        <li><a href="#">2</a></li>
                                                        <li><a href="#">3</a></li>
                                                        <li><a href="#">4</a></li>
                                                        <li><a href="#"><i class="lnr lnr-chevron-right"></i></a></li>
                                                    </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--pagination ends-->
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>





@endsection
@section('footer_script')

    <script type="text/javascript">
        $(document).ready(function () {
            $("select#city_id").on('change', function (e) {
                var city_id = $(this).children('option:selected').attr('value');
                if (city_id != undefined) {
                    window.location.href = "{{ url('/property/list') }}?city_id=" + city_id;
                } else {
                    window.location.href = "{{ url('/property/list') }}";
                }
            });
        })
    </script>

@endsection
