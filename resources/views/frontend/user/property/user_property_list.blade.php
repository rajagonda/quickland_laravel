<!doctype html>
<html>

<head>
    @include('frontend.includes.fronthead')
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="{{ asset('/css/image-uploader.css') }}" rel="stylesheet" />
</head>

<body>
    <div class="page-wrapper">
        @include('frontend.includes.frontheader')

        
        <div class="filter-wrapper style1">
            <div class="container bg-add-listing">
                <div class="row">
                    <div class="col-xl-3 col-lg-12">
                        <!--Sidebar starts-->
                        <div class="sidebar-left">
                            <div class="widget categories">
                                <h3 class="widget-title">Dashboard</h3>
                                <ul class="icon">
                                    <li><a href="#" >Home</a></li>
                                    <li><a href="{{ url('/user/property/list') }}" class="active">Property Listing</a></li>
                                    <li><a href="#">Leads</a></li>
                                    <li><a href="#">My Profile</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--Sidebar ends-->
                    <div class="col-xl-9 col-lg-12">
                        <div class="sidebar-content-right py-100">
                            <ul class="nav nav-tabs list-details-tab">
                                <li class="nav-item active">
                                    <a data-toggle="tab" href="#all_property">All Property</a>
                                </li>
                            </ul>
                            <div class="item-wrapper pt-20    ">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade  show active  property-list" id="list-view">
                                        @if(!empty($data) && $data->count())
                                        @foreach($data as $key => $value)
                                        <div class="single-property-box">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="property-item">
                                                        <a class="property-img" href="{{ url('/property/show') }}/{{ $value->property_id}}/2">
                                                            @php
                                                            $images = explode(',',$value->new_property_files);
                                                            $first_image_name = $images[0];
                                                            @endphp
                                                            <img src="{{ asset('/property_photos')}}/{{$value->property_id}}/{{$first_image_name}}" alt="#">
                                                        </a>
                                                        <ul class="feature_text">
                                                            <li class="feature_cb"><span> Featured</span></li>
                                                            <li class="feature_or"><span>For Sale</span></li>
                                                        </ul>
                                                        <div class="property-author-wrap">
                                                            <a href="#" class="property-author">
                                                                <img src="images/agents/agent_min_1.jpg" alt="...">
                                                                <span>Agent Name</span>
                                                            </a>
                                                            <ul class="save-btn">
                                                                <li data-toggle="tooltip" data-placement="top" title="Photos"><a href=".photo-gallery{{$key}}" class="btn-gallery"><i class="lnr lnr-camera"></i></a></li>
                                                                <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                                            </ul>
                                                            <div class="hidden photo-gallery{{$key}}">
                                                                @foreach($images as $img)
                                                                <a href="{{ asset('/property_photos')}}/{{$value->property_id}}/{{$img}}"></a>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 col-sm-12">
                                                    <div class="property-title-box">
                                                        <h4><a href="{{ url('/property/show') }}/{{ $value->property_id}}/1">{{$value->project_name}}</a></h4>
                                                        <div class="property-location">
                                                            <i class="fa fa-map-marker-alt"></i>
                                                            <p>{{$value->locality}}</p>
                                                        </div>
                                                        <ul class="property-feature">
                                                            <li><span class="trend-open"><p>₹{{$value->plot_price}} Lacs</p></span></li>
                                                            <li><span>{{$value->plot_area}} sq ft</span></li>
                                                        </ul>
                                                        <div class="property-desc">
                                                            <p>{{$value->about_property}} <label class="trend-open">...more</label></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="property-partition"></div>
                                                <div class="col-md-9 col-sm-12">
                                                    <div class="property-btm-info">
                                                        <span>Posted 3 days ago by Owner</span>
                                                        <span>Green City</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12">
                                                    <div class="property-contact">
                                                        <button class="btn v8" type="submit">Contact Owner</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @endif
                                        
                                    </div>
                                    <!--pagination starts-->
                                    <div class="post-nav nav-res pt-50  ">
                                        <div class="row">
                                            <div class="col-md-8 offset-md-2  col-xs-12 ">
                                                <div class="page-num text-center">
                                                    @if ($data->lastPage() > 1)
<ul class="">
    <li class="{{ ($data->currentPage() == 1) ? ' disabled' : '' }}">
        <a href="{{ $data->url(1) }}"><i class="lnr lnr-chevron-left"></i></a>
    </li>
    @for ($i = 1; $i <= $data->lastPage(); $i++)
        <li class="{{ ($data->currentPage() == $i) ? ' active' : '' }}">
            <a href="{{ $data->url($i) }}">{{ $i }}</a>
        </li>
    @endfor
    <li class="{{ ($data->currentPage() == $data->lastPage()) ? ' disabled' : '' }}">
        <a href="{{ $data->url($data->currentPage()+1) }}" ><i class="lnr lnr-chevron-right"></i></a>
    </li>
</ul>
@endif
                                                    <!-- <ul>
                                                        <li class="active"><a href="#">1</a></li>
                                                        <li><a href="#">2</a></li>
                                                        <li><a href="#">3</a></li>
                                                        <li><a href="#">4</a></li>
                                                        <li><a href="#"><i class="lnr lnr-chevron-right"></i></a></li>
                                                    </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--pagination ends-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Listing Filter ends-->
    <!-- Scroll to top starts-->
    <span class="scrolltotop"><i class="lnr lnr-arrow-up"></i></span>
    <!-- Scroll to top ends-->
</div>
<!--Page Wrapper ends-->

@include('frontend.includes.login-register')
@include('frontend.includes.footer')
<script src="{{ asset('/js/image-uploader.js') }}"></script>
<script src="{{ asset('/js/basic_info_validation.js') }}"></script>
<script src="{{ asset('/js/location_info_validation.js') }}"></script>
<script src="{{ asset('/js/property_info_validation.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(e){

        $('.input-images-1').imageUploader();

        $('#p_2').change(
            function(){
                if ($(this).is(':checked') ) { 
                    $("div#p_calender").show();
                } else { 
                    $("div#p_calender").hide(); 
                }
            });
        $('#p_1').change(
            function(){
                if ($(this).is(':checked') ) {

                 $("div#p_calender").hide();
             } 
         });

        $('#p_type_2').change(
            function(){
                if ($(this).is(':checked') ) { 
                    $("div#Residential").hide();
                } else { 
                    $("div#Residential").show(); 
                }
            });
        $('#p_type_1').change(
            function(){
                if ($(this).is(':checked') ) {

                 $("div#Residential").show();
             } 
         });

        $("a#basic_cont_btn").on('click',function(e){
            basic_info_validation(e);
        })

        $("a#location-cont-btn").on('click',function(e){

            location_info_validation(e)
            
        });

        $("a#prop_info_cont_btn").on('click',function(e){

            property_info_validation(e)
        });

        $("a#amenities_btn").on('click',function(e){

            e.preventDefault();
            $("li#upload-photos > a").click();
        });

        $("select#state_list").on('change',function(e){
            var stateSel = $("option:selected", this);
            var stateVal = this.value;

            $.ajax({
                type:'GET',
                url : "{{ url('/ajax/get_cities_of_state') }}/"+stateVal,
                success : function(res)
                {
                    var get_res = JSON.parse(res);
                    console.log(get_res);
                    if(get_res.status)
                    {
                        $( "select#city_list" ).find('option').not(':first').remove();
                        var cities = get_res.cities;

                        for(i = 0; i < cities.length; i++)
                        {
                            $("select#city_list").append("<option value='"+cities[i]['city_id']+"'>"+cities[i]['city']+"</option>")

                        }
                        $( "select#city_list" ).niceSelect('update');
                    }
                    else
                    {

                    }
                }
            })
        })

        $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-success").slideUp(500);
        });
    })
</script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_8C7p0Ws2gUu7wo0b6pK9Qu7LuzX2iWY&callback=initialize"></script> -->
</body>

</html>
