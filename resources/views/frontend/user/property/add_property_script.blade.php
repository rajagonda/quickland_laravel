<script type="text/javascript">

    function setp1_Valiodation() {
        $('#post_prop_form_general_info').validate({
            ignore: [],
            // errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            // errorElement: 'div',

            errorClass: 'help-block animation-slideDown text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },

            rules: {

                user_type_id: {
                    required: true,
                },

                about_you: {
                    required: true,
                },

                use_type_id: {
                    required: true,
                },

                property_type_id: {
                    required: true,
                },

                property_sub_type_id: {
                    required: true,
                },

                available_for: {
                    required: true,
                },

                possession: {
                    required: true,
                },

                possession_date: {
                    required: true,
                },


            },
            messages: {

                user_type_id: {
                    required: 'Select  User Type',
                },

                about_you: {
                    required: 'Enter About Informations',
                },

                use_type_id: {
                    required: 'Select Type',
                },

                property_type_id: {
                    required: 'Select Property Type',
                },

                property_sub_type_id: {
                    required: 'Select Property Sub Type',
                },

                available_for: {
                    required: 'Select Available',
                },

                possession: {
                    required: 'Select Possession',
                },

                possession_date: {
                    required: 'Select Possession Date',
                },


            },
            submitHandler: function (form) {
                console.log('fom submit')
                return true; // required to block normal submit since you used ajax
            }
        });
    }

    function setp2_locations_Valiodation() {
        $('#setp2_locations_Valiodation').validate({
            ignore: [],
            // errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            // errorElement: 'div',

            errorClass: 'help-block animation-slideDown text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },

            rules: {

                locality: {
                    required: true,
                },

                state_id: {
                    required: true,
                },

                city_id: {
                    required: true,
                },

                project_name: {
                    required: true,
                },

                property_address: {
                    required: true,
                },




            },
            messages: {

                locality: {
                    required: 'Select  User Type',
                },

                state_id: {
                    required: 'Enter About Informations',
                },

                city_id: {
                    required: 'Select Type',
                },

                project_name: {
                    required: 'Select Property Type',
                },

                property_address: {
                    required: 'Select Property Sub Type',
                },


            },
            submitHandler: function (form) {
                console.log('fom submit')
                return true; // required to block normal submit since you used ajax
            }
        });
    }


    function getPropertySubtypes(active_id = null) {
        var value = $("[name=property_type_id]:checked").val();
        // console.log(value)
        var postdata = {id: value, action: 'property_type_sub1', 'active_id': active_id};
        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            url: "{{ route('site.ajax.property.opt') }}",
            data: postdata,
            success: function (res) {
                console.log(res)
                $("#property_type_sub1").html("" + res);
            }
        });

    }


    $('.property_type_id').on('click', function () {
        getPropertySubtypes();
    })

    function possessionsGetgate(possession_date = null) {
        var value = $("[name=possession_id]:checked").val();

        console.log(value)

        var postdata = {id: value, action: 'possessions_date', 'possession_date': possession_date};


        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            url: "{{ route('site.ajax.property.opt') }}",
            data: postdata,
            success: function (res) {
                console.log(res)


                // $("#property_type_sub1").html("");
                $("#possession_calender").html("" + res);


            }
        });

    }

    $('.possessions_input').on('click', function () {
        possessionsGetgate();
    });


    $(document).ready(function (e) {
        console.log('fom submit')

        setp1_Valiodation();
        setp2_locations_Valiodation();

        getPropertySubtypes({{ isset($property_info->property_sub_type_id) ? $property_info->property_sub_type_id:'' }});
        possessionsGetgate("{{ isset($property_info->possession_date) ? $property_info->possession_date:'' }}");
    });

</script>