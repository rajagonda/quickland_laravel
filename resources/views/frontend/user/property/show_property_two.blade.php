<!doctype html>
<html>

<head>
	@include('frontend.includes.single-listing-head')
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	
</head>

<body>
	<!--Preloader starts-->

	<!--Preloader ends-->
	<!--Page Wrapper starts-->
	<div class="page-wrapper">
		<!--Header starts-->
		@include('frontend.includes.single-listing-header')
		<!--Header ends-->
		<div class="property-details-wrap bg-cb">
			<!--Listing Details Hero ends-->
			<div class="single-property-header v3">
				<div class="video-container">
					<div class="overlay op-3"></div>
					@php
					$images = explode(',',$prop_details->new_property_files);
					$image_one = $prop_details->property_id.'/'.$images[0];

					@endphp

					<div id="video-wrapper" style="background-image: 
					{{ asset('/property_photos') }}/{{$image_one}} "> </div>
					<a class="player" data-property="{videoURL:'{{ $prop_details->video_url }}'}"></a>
				</div>
			</div>
			<!--Listing Details Hero ends-->
			<!--Listing Details Info starts-->
			<div class="single-property-details mt-50">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="list-details-title v3">
								<div class="row">
									<div class="col-lg-6 col-md-7 col-sm-12">
										<div class="single-listing-title float-left">
											<h2>{{ $prop_details->project_name }}<span class="btn v5">For {{ $prop_details->use_type }}</span></h2>
											<p><i class="fa fa-map-marker-alt"></i> {{ $prop_details->property_address }}</p>
											<!-- <a href="#" class="property-author">
												<img src="" alt="...">
												<span>Zilion Properties</span>
											</a> -->
										</div>
									</div>
									<div class="col-lg-6 col-md-5 col-sm-12">
										<div class="list-details-btn text-right sm-left">
											<div class="trend-open">
												@if($prop_details->use_type == 'Sell')
												<p>{{ $prop_details->price_per_sq_feet }}<span class="per_month">{{ $prop_details->plot_measure }}</span> </p>
												@endif
											</div>
											<div class="list-ratings">
												<span class="fas fa-star"></span>
												<span class="fas fa-star"></span>
												<span class="fas fa-star"></span>
												<span class="fas fa-star"></span>
												<span class="fas fa-star-half-alt"></span>
											</div>
											<ul class="list-btn">
												<li class="share-btn">
													<a href="#" class="btn v2" data-toggle="tooltip" title="Share"><i class="fas fa-share-alt"></i></a>
													<ul class="social-share">
														<li class="bg-fb"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
														<li class="bg-tt"><a href="#"><i class="fab fa-twitter"></i></a></li>
														<li class="bg-ig"><a href="#"><i class="fab fa-instagram"></i></a></li>
													</ul>
												</li>
												<li class="save-btn">
													<a href="#" class="btn v2" data-toggle="tooltip" title="Save"><i class="fa fa-heart"></i></a>
												</li>
												<li class="print-btn">
													<a href="#" class="btn v2" data-toggle="tooltip" title="Print"><i class="lnr lnr-printer"></i></a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xl-8 col-lg-12">
							<div class="listing-desc-wrap mr-30">
								<div id="list-menu" class="list_menu">
									<ul class="list-details-tab fixed_nav" style="width: 678.5px;">
										<li class="nav-item active"><a href="#description" class="active">Description</a></li>
										<li class="nav-item"><a href="#location">Location</a></li>
										<li class="nav-item"><a href="#gallery">Gallery</a></li>
										<li class="nav-item"><a href="#details">Details</a></li>
										<!-- <li class="nav-item"><a href="#floor_plan">Floor plan</a></li> -->
										<!-- <li class="nav-item"><a href="#nearby">Nearby</a></li> -->
										<li class="nav-item"><a href="#enquiry">Enquiry</a></li>
										
									</ul>
								</div>
								<!--Listing Details starts-->
								<div  class="list-details-wrap">
									<div id="description" class="list-details-section">
										<h4>Property Brief</h4>
										<div class="overview-content">
											<p class="mb-10">{{ $prop_details->about_property }}</p>
										</div>
										
									</div>
									<div id="location" class="list-details-section">
										<h4 class="list-subtitle">Location</h4>
										<a target="_blank" href="http://maps.google.com/?q=4936%20N%20Broadway%20St,%20Chicago,%20IL%2060640,%20USA" class="location-map">View Map<i class="fa fa-map-marker-alt"></i></a>
										<ul class="listing-address">
											<li>Address : <span>{{ $prop_details->locality }}</span></li>
											<li>Country : <span>India</span></li>
											<li>State : <span>{{ $prop_details->state }}</span></li>

											<!-- <li>Zip/Postal Code : <span>4203</span></li> -->
											<li>City : <span>{{ $prop_details->city }}</span></li>
										</ul>
									</div>
									<div id="gallery" class="list-details-section">
										<h4>Gallery</h4>
										<!--Carousel Wrapper-->
										<div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails list-gallery pt-2" data-ride="carousel">
											<!--Slides-->
											<div class="carousel-inner" role="listbox">
												<div class="carousel-item active">
													<img class="d-block w-100" src="{{ asset('/property_photos') }}/{{$image_one}}" alt="slide">
												</div>
												@php
												for($i=1; $i < count($images); $i++){


												$image = $prop_details->property_id.'/'.$images[$i];
												@endphp

												<div class="carousel-item">
													<img class="d-block w-100" src="{{ asset('/property_photos') }}/{{$image}}" alt="slide">
												</div>
												@php
											}
											@endphp

										</div>
										<!--Controls starts-->
										<a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
											<span class="lnr lnr-arrow-left" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										</a>
										<a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
											<span class="lnr lnr-arrow-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										</a>
										<!--Controls ends-->
										<ol class="carousel-indicators  list-gallery-thumb">
											@php
											for($i=0; $i < count($images); $i++){


											$image = $prop_details->property_id.'/'.$images[$i];
											@endphp
											<li data-target="#carousel-thumb" data-slide-to="{{ $i }}"><img class="img-fluid d-block w-100" src="{{ asset('/property_photos') }}/{{$image}}" alt="..."></li>

											@php
										}
										@endphp
									</ol>
								</div>
								<!--/.Carousel Wrapper-->
							</div>

							<div id="details" class="list-details-section">
								<div class="mb-40">
									<h4>Property Details</h4>
									<ul class="property-info">
										<li>Property ID : <span>{{ $prop_details->property_id }}</span></li>
										<li>Property Type : <span>{{ $prop_details->property_type }}</span></li>
										<li>Property status : <span>For rent</span></li>
										<li>Property Price : <span>{{ $prop_details->plot_price }}</span></li>


									</ul>
								</div>
								<div class="mb-40">
									<h4>Amenities</h4>
									@php
									$amenities['basketball'] = '<li><i class="fas fa-basketball-ball"></i>basketball court</li>';
									$amenities['gym'] = '<li><i class="fas fa-dumbbell"></i> Gym</li>';
									$amenities['washer'] = '<li><i class="fas fa-dumpster"></i>washer and dryer </li>';
									$amenities['wheelchair'] = '<li><i class="fab fa-accessible-icon"></i> Wheelchair Friendly</li>';
									$amenities['aircondition'] = '<li><i class="fas fa-fan"></i>Air Conditioned</li>';
									$amenities['swimmingpool'] = '<li><i class="fas fa-swimmer"></i>Swimming Pool </li>';


									$prop_aminities = explode(',',$prop_details->amenities);
									@endphp
									<ul class="listing-features">
										@php
										foreach($prop_aminities as $pa)
										{
											echo $amenities[$pa];
										}
										@endphp

									</ul>
								</div>
									<!-- <div>
										<h4>Property Documents</h4>
										<ul class="listing-features pp_docs">
											<li><a target="_blank" href="images/property-file/demo.docx"><i class="lnr lnr-file-empty"></i>Sample Property Document </a></li>
										</ul>
									</div> -->

								</div>
								<!--<div id="floor_plan" class="list-details-section">
									<h4>Floor Plans</h4>
									<div id="accordion10" role="tablist" class="pt-2">
										<div class="card">
											<div class="card-header" role="tab" id="headingOne">
												<a role="button" data-toggle="collapse" data-parent="#accordion10" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
													<p>First Floor</p>
													<i class="fas fa-angle-down"></i>
													<ul class="floor-list">
														<li>Size : <span>1267 sft</span></li>
														<li>Rooms : <span>144 sft</span></li>
														<li>Baths : <span>48 sft</span></li>
														<li>Price : <span>$1200</span></li>
													</ul>
												</a>
											</div>
											<div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne">
												<div class="card-body">
													<a href="" data-lightbox="single-1">
														<img src="" alt="...">
													</a>
												</div>
											</div>
										</div>
										<div class="card">
											<div class="card-header" role="tab" id="headingTwo">
												<a role="button" data-toggle="collapse" data-parent="#accordion10" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
													<p>Second Floor</p>
													<i class="fas fa-angle-down"></i>
													<ul class="floor-list">
														<li>Size : <span>1500 sft</span></li>
														<li>Rooms : <span>150 sft</span></li>
														<li>Baths : <span>88 sft</span></li>
														<li>Price : <span>$1500</span></li>
													</ul>
												</a>
											</div>
											<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
												<div class="card-body">
													<a href="" data-lightbox="single-1">
														<img src="" alt="...">
													</a>
												</div>
											</div>
										</div>
									</div>
								</div> -->
								<!-- <div id="nearby" class="list-details-section">
									<div class="container no-pad-lr">
										<h4>What's Nearby?</h4>
										<div class="nearby-wrap pt-2">
											<h4><span><i class="fas fa-utensils"></i></span>Food</h4>
											<div class="row align-items-center">
												<div class="col-md-6 col-6">
													<p class="nearby-place">KFC (0.03mile)</p>
												</div>
												<div class="col-md-6 col-6">
													<ul class="product-rating float-right nearby-rating">
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star-half-alt"></i></li>
														<li><i class="fas fa-star-half-alt"></i></li>
													</ul>
												</div>
											</div>
											<div class="row align-items-center">
												<div class="col-md-6 col-6">
													<p class="nearby-place">KFC (0.03mile)</p>
												</div>
												<div class="col-md-6 col-6">
													<ul class="product-rating float-right nearby-rating">
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star-half-alt"></i></li>
														<li><i class="fas fa-star-half-alt"></i></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="nearby-wrap">
											<h4><span><i class="fas fa-university"></i></span>Education</h4>
											<div class="row align-items-center">
												<div class="col-md-6 col-7">
													<p class="nearby-place">Oakleaf School(0.01mile)</p>
												</div>
												<div class="col-md-6 col-5">
													<ul class="product-rating float-right nearby-rating">
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star-half-alt"></i></li>
														<li><i class="fas fa-star-half-alt"></i></li>
													</ul>
												</div>
											</div>
											<div class="row align-items-center">
												<div class="col-md-6 col-7">
													<p class="nearby-place">Frozen Lake Academy (0.1mile)</p>
												</div>
												<div class="col-md-6 col-5">
													<ul class="product-rating float-right nearby-rating">
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star-half-alt"></i></li>
														<li><i class="fas fa-star-half-alt"></i></li>
													</ul>
												</div>
											</div>
											<div class="row align-items-center">
												<div class="col-md-6 col-7">
													<p class="nearby-place">Spring Gardens Elementary(0.05mile)</p>
												</div>
												<div class="col-md-6 col-5">
													<ul class="product-rating float-right nearby-rating">
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star-half-alt"></i></li>
														<li><i class="fas fa-star-half-alt"></i></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="nearby-wrap">
											<h4><span><i class="fas fa-medkit"></i></span>Health</h4>
											<div class="row align-items-center">
												<div class="col-md-6 col-7">
													<p class="nearby-place">Basuda Health Care (0.03mile)</p>
												</div>
												<div class="col-md-6 col-5">
													<ul class="product-rating float-right nearby-rating">
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star"></i></li>
														<li><i class="fas fa-star-half-alt"></i></li>
														<li><i class="fas fa-star-half-alt"></i></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div> -->
								<div id="enquiry" class="list-details-section">
									<h4 class="list-details-title">Enquiry</h4>
									<!-- <form method="POST" class="tour-form" name="enquiry_form" id="enquiry_form"> -->
										<input id="enquiry_csrf" type="hidden" name="csrf-token" value="{{ csrf_token() }}" />
                                                            
										<div class="row">
											
											
											<div class="col-md-8">
												<input name="cust_name" id="cust_name" type="text" class="form-control filter-input" placeholder="Your name">
											</div>
											<div class="col-md-8">
												<input name="cust_phone" id="cust_phone" type="text" class="form-control filter-input" placeholder="Your Phone">
											</div>
											<div class="col-md-8">
												<input name="cust_email" id="cust_email" type="text" class="form-control filter-input" placeholder="Your email">
											</div>
											<div class="col-md-10" style="margin-top: 23px;">
												<textarea class="contact-form__textarea mb-5" name="comment" id="comment" placeholder="Your Message"></textarea>
												<input class="btn v3" type="submit" name="submit-contact" id="submit_enquiry" value="Submit" style="margin-top: 20px;">
											</div>
										</div>

									<!-- </form> -->
								</div>
								<!-- <div id="reviews" class="list-details-section mt-10">
									<h4>Reviews <span>(2)</span></h4>
									<div class="review-box comment-box">
										<ul class="review_wrap">
											<li>
												<div class="customer-review_wrap">
													<div class="reviewer-img">
														<img src="" class="img-fluid" alt="#">
														<p>Stacy G.</p>
													</div>
													<div class="customer-content-wrap">
														<div class="customer-content">
															<div class="customer-review">
																<h6>Lorem ipsum dolor.</h6>

																<p class="comment-date">
																	Jun 13, 2019
																</p>
																<ul class="product-rating">
																	<li><i class="fas fa-star"></i></li>
																	<li><i class="fas fa-star"></i></li>
																	<li><i class="fas fa-star"></i></li>
																	<li><i class="fas fa-star-half-alt"></i></li>
																	<li><i class="fas fa-star-half-alt"></i></li>
																</ul>
															</div>
														</div>
														<p class="customer-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium voluptates eum, velit recusandae, ducimus earum aperiam commodi error officia optio aut et quae nam nostrum!
														</p>
														<div class="like-btn">
															<a href="#" class="rate-review float-right"><i class="fas fa-reply"></i>Reply</a>
														</div>
													</div>
												</div>
												<ul class="review_wrap has-child">
													<li>
														<div class="customer-review_wrap">
															<div class="reviewer-img">
																<img src="" class="img-fluid" alt="#">
																<p>Tony S.</p>
															</div>
															<div class="customer-content-wrap">
																<div class="customer-content">
																	<div class="customer-review">
																		<h6>Lorem ipsum dolor sit.</h6>
																		<p class="comment-date">
																			Jun 14, 2019
																		</p>
																	</div>
																</div>
																<p class="customer-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore quod, nihil consectetur sit natus eum similique quae omnis doloribus culpa!
																</p>
																<div class="like-btn">
																	<a href="#" class="rate-review float-right"><i class="fas fa-reply"></i>Reply</a>
																</div>
															</div>
														</div>
													</li>
												</ul>
											</li>
											<li>
												<div class="customer-review_wrap">
													<div class="reviewer-img">
														<img src="" class="img-fluid" alt="#">
														<p>Louis F.</p>
													</div>
													<div class="customer-content-wrap">
														<div class="customer-content">
															<div class="customer-review">
																<h6>Lorem ipsum dolor.</h6>
																<p class="comment-date">
																	Mar 02, 2019
																</p>
																<ul class="product-rating">
																	<li><i class="fas fa-star"></i></li>
																	<li><i class="fas fa-star"></i></li>
																	<li><i class="fas fa-star"></i></li>
																	<li><i class="fas fa-star-half-alt"></i></li>
																	<li><i class="fas fa-star-half-alt"></i></li>
																</ul>

															</div>
														</div>
														<p class="customer-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium voluptates eum, velit recusandae, ducimus earum aperiam commodi error officia optio aut et quae nam nostrum!
														</p>
														<div class="like-btn">
															<a href="#" class="rate-review float-right"><i class="fas fa-reply"></i>Reply</a>
														</div>
													</div>
												</div>
											</li>
										</ul>
										<form class="contact-form" id="leave-review">
											<h4 class="contact-form__title">
												Rate us and Write a Review
											</h4>
											<div class="row">
												<div class="col-md-6 col-sm-7 col-12">
													<p class="contact-form__rate">
														Your rating for this listing:
													</p>
													<p class="contact-form__rate-bx">
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
													</p>
													<p class="contact-form__rate-bx-show">
														<span class="rate-actual">0</span> / 5
													</p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<input class="contact-form__input-text" type="text" name="name" id="name" placeholder="Name:">
												</div>
												<div class="col-md-6">
													<input class="contact-form__input-text" type="text" name="mail" id="mail" placeholder="Email:">
												</div>
											</div>
											<textarea class="contact-form__textarea" name="comment" id="comment_msg" placeholder="Comment"></textarea>
											<input class="btn v3" type="submit" name="submit-contact"  value="Submit">
										</form>
									</div>
								</div> -->
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-12">
						<div id="list-sidebar" class="listing-sidebar">
							<!-- <div class="widget">
								<div class="row">
									<div class="col-4">
										<img class="agency-chat-img" src="" alt="...">
									</div>
									<div class="col-8 pl-0">
										<ul class="agent-chat-info">
											<li><i class="fas fa-user"></i>Tony Stark</li>
											<li><i class="fas fa-phone-alt"></i>+440 3456 345</li>
											<li><a href="single-agent.html">View Listings</a></li>
										</ul>
									</div>
								</div>
								<div class="row mt-1">
									<div class="col-md-12">
										<form action="#" method="POST">
											<div class="chat-group mt-1">
												<input class="chat-field" type="text" name="chat-name" id="chat-name" placeholder="Your name">
											</div>
											<div class="chat-group mt-1">
												<input class="chat-field" type="text" name="chat-phone" id="chat-phone" placeholder="Phone">
											</div>
											<div class="chat-group mt-1">
												<input class="chat-field" type="text" name="chat-email" id="chat-email" placeholder="Email">
											</div>
											<div class="chat-group mt-1">
												<textarea class="form-control chat-msg" name="message" rows="4" placeholder="Description">Hello, I am interested in [Luxury apartment bay view]</textarea>
											</div>
											<div class="chat-button mt-3">
												<a class="chat-btn" data-toggle="modal" data-target="#mortgage_result">Send Message</a>
											</div>
										</form>
									</div>
								</div>
							</div> -->
							<div class="widget">
								<h3 class="widget-title">Recently Added</h3>
								<li class="row recent-list">
									<div class="col-lg-5 col-4">
										<div class="entry-img">
											<img src="" alt="...">
											<span>For Rent</span>
										</div>
									</div>
									<div class="col-lg-7 col-8 no-pad-left">
										<div class="entry-text">
											<h4 class="entry-title"><a href="single-news-one.html">Sea View Villa</a></h4>
											<div class="property-location">
												<i class="fa fa-map-marker-alt"></i>
												<p>1797 Hillcrest Lane, Irvine, CA</p>
											</div>
											<div class="trend-open">
												<p>$7500<span class="per_month">month</span></p>
											</div>
										</div>
									</div>
								</li>
								<li class="row recent-list">
									<div class="col-lg-5 col-4">
										<div class="entry-img">
											<img src="" alt="...">
											<span>For Sale</span>
										</div>
									</div>
									<div class="col-lg-7 col-8 no-pad-left">
										<div class="entry-text">
											<h4 class="entry-title"><a href="single-news-one.html">Apartment on Glasgow</a></h4>
											<div class="property-location">
												<i class="fa fa-map-marker-alt"></i>
												<p>60 High St, Glasgow, London</p>
											</div>
											<div class="trend-open">
												<p><span class="per_sale">starts from</span>$10000</p>
											</div>
										</div>
									</div>
								</li>
								<li class="row recent-list">
									<div class="col-lg-5 col-4">
										<div class="entry-img">
											<img src="" alt="...">
											<span>For Rent</span>
										</div>
									</div>
									<div class="col-lg-7 col-8 no-pad-left">
										<div class="entry-text">
											<h4 class="entry-title"><a href="single-news-one.html">Family house in Florance.</a></h4>
											<div class="property-location">
												<i class="fa fa-map-marker-alt"></i>
												<p>4210 Kha St,Florence,USA</p>
											</div>
											<div class="trend-open">
												<p>$4500<span class="per_month">month</span></p>
											</div>
										</div>
									</div>
								</li>
							</div>
							<!-- <div class="widget">
								<h3 class="widget-title">Featured Property</h3>
								<div class="swiper-container single-featured-list">
									<div class="swiper-wrapper">
										<div class="swiper-slide single-property-box">
											<div class="property-item">
												<a class="property-img" href="single-listing-two.html"><img src="" alt="#">
												</a>
												<ul class="feature_text">
													<li class="feature_cb"><span> Featured</span></li>
													<li class="feature_or"><span>For Sale</span></li>
												</ul>
												<div class="property-author-wrap">
													<h4><a href="#" class="property-author">
														Sea view Villa
													</a>
												</h4> 
												<div class="featured-price">
													<p><span class="per_sale">starts from</span>$12000</p>
												</div>
											</div>
										</div>
									</div>
									<div class="swiper-slide single-property-box">
										<div class="property-item">
											<a class="property-img" href="single-listing-two.html"><img src="" alt="#">
											</a>
											<ul class="feature_text">
												<li class="feature_cb"><span> Featured</span></li>
												<li class="feature_or"><span>For Rent</span></li>
											</ul>
											<div class="property-author-wrap">
												<a href="#" class="property-author">
													<span>Awesome Family home</span>
												</a>
												<div class="featured-price">
													<p>$2300<span class="per_month">month</span></p>
												</div>
											</div>
										</div>
									</div>
									<div class="swiper-slide single-property-box">
										<div class="property-item">
											<a class="property-img" href="single-listing-two.html"><img src="" alt="#">
											</a>
											<ul class="feature_text">
												<li class="feature_cb"><span> Featured</span></li>
												<li class="feature_or"><span>For Sale</span></li>
											</ul>
											<div class="property-author-wrap">
												<a href="#" class="property-author">
													<span>Condo in Hartland</span>
												</a>
												<div class="featured-price">
													<p><span class="per_sale">starts from</span>$23000</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="slider-btn v3 single-featured-next"><i class="lnr lnr-arrow-right"></i></div>
								<div class="slider-btn v3 single-featured-prev"><i class="lnr lnr-arrow-left"></i></div>
							</div>
						</div> -->
						<!-- <div class="widget mortgage-widget">
							<h3 class="widget-title">Property Mortgage</h3>
							<form action="#" method="GET" enctype="multipart/form-data">
								<div class="mortgage-group">
									<span class="mortgage-icon">$</span>
									<input class="mortgage-field" type="text" name="amount-one" id="amount-one" placeholder="Total Amount">
								</div>
								<div class="mortgage-group">
									<span class="mortgage-icon">$</span>
									<input class="mortgage-field" type="text" name="amount-two" id="amount-two" placeholder="Down Payment">
								</div>
								<div class="mortgage-group">
									<span class="mortgage-icon">$</span>
									<input class="mortgage-field" type="text" name="amount-three" id="amount-three" placeholder="Loan Terms(Years)">
								</div>
								<div class="mortgage-group">
									<span class="mortgage-icon">%</span>
									<input class="mortgage-field" type="text" name="amount-four" id="amount-four" placeholder="Total Amount">
								</div>
								<div class="mortgage-btn">
									<button type="submit" data-toggle="modal" data-target="#mortgage_result">Calculate</button>
								</div>
							</form>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Listing Details Info ends-->
	<!--Similar Listing starts-->
	<div class="similar-listing-wrap mt-70 pb-70">
		<div class="container">
			<div class="col-md-12 px-0">
				<!-- <div class="similar-listing">
					<div class="section-title v2">
						<h2>Similar Listings</h2>
					</div>
					<div class="swiper-container similar-list-wrap">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<div class="single-property-box">
									<div class="property-item">
										<a class="property-img" href="single-listing-two.html"><img src="" alt="#"> </a>
										<ul class="feature_text">
											<li class="feature_cb"><span>New</span></li>
											<li class="feature_or"><span>For Rent</span></li>
										</ul>
										<div class="property-author-wrap">
											<a href="#" class="property-author">
												<img src="" alt="...">
												<span>Carmen Properties</span>
											</a>
											<ul class="save-btn">
												<li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
												<li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="property-title-box">
										<h4><a href="single-listing-one.html">Villa on Sunbury</a></h4>
										<div class="property-location">
											<i class="fa fa-map-marker-alt"></i>
											<p>39 Casey Ave, Sunbury, VIC 3429</p>
										</div>
										<ul class="property-feature">
											<li> <i class="fas fa-bed"></i>
												<span>5 Bedrooms</span>
											</li>
											<li> <i class="fas fa-bath"></i>
												<span>4 Bath</span>
											</li>
											<li> <i class="fas fa-arrows-alt"></i>
												<span>2048 sq ft</span>
											</li>
											<li> <i class="fas fa-car"></i>
												<span>2 Garage</span>
											</li>
										</ul>
										<div class="trending-bottom">
											<div class="trend-left float-left">
												<ul class="product-rating">
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star-half-alt"></i></li>
													<li><i class="fas fa-star-half-alt"></i></li>
												</ul>
											</div>
											<a class="trend-right float-right">
												<div class="trend-open">
													<p>$9200<span class="per_month">month</span> </p>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="single-property-box">
									<div class="property-item">
										<a class="property-img" href="single-listing-two.html"><img src="" alt="#"> </a>
										<ul class="feature_text">
											<li class="feature_cb"><span>New</span></li>
											<li class="feature_or"><span>For Rent</span></li>
										</ul>
										<div class="property-author-wrap">
											<a href="#" class="property-author">
												<img src="" alt="...">
												<span>Bob haris</span>
											</a>
											<ul class="save-btn">
												<li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
												<li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="property-title-box">
										<h4><a href="single-listing-one.html">Bay view Apartment</a></h4>
										<div class="property-location">
											<i class="fa fa-map-marker-alt"></i>
											<p>1797 Hillcrest Lane, Boulevard, CA</p>
										</div>
										<ul class="property-feature">
											<li> <i class="fas fa-bed"></i>
												<span>3 Bedrooms</span>
											</li>
											<li> <i class="fas fa-bath"></i>
												<span>2 Bath</span>
											</li>
											<li> <i class="fas fa-arrows-alt"></i>
												<span>1400 sq ft</span>
											</li>
											<li> <i class="fas fa-car"></i>
												<span>1 Garage</span>
											</li>
										</ul>
										<div class="trending-bottom">
											<div class="trend-left float-left">
												<ul class="product-rating">
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star-half-alt"></i></li>
													<li><i class="fas fa-star-half-alt"></i></li>
												</ul>
											</div>
											<a class="trend-right float-right">
												<div class="trend-open">
													<p>$4000<span class="per_month">month</span> </p>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="single-property-box">
									<div class="property-item">
										<a class="property-img" href="single-listing-two.html"><img src="" alt="#"> </a>
										<ul class="feature_text">
											<li class="feature_cb"><span>New</span></li>
											<li class="feature_or"><span>For Sale</span></li>
										</ul>
										<div class="property-author-wrap">
											<a href="#" class="property-author">
												<img src="" alt="...">
												<span>Tony Stark</span>
											</a>
											<ul class="save-btn">
												<li data-toggle="tooltip" data-placement="top" title="Photos"><a href=".photo-gallery" class="btn-gallery"><i class="lnr lnr-camera"></i></a></li>
												<li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
												<li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
											</ul>
											<div class="hidden photo-gallery">
												<a href=""></a>
												<a href=""></a>
												<a href=""></a>
												<a href=""></a>
												<a href=""></a>
												<a href=""></a>
												<a href=""></a>
											</div>
										</div>
									</div>
									<div class="property-title-box">
										<h4><a href="single-listing-one.html">Villa on Hartford</a></h4>
										<div class="property-location">
											<i class="fa fa-map-marker-alt"></i>
											<p>2854 Meadow View Drive, Hartford, USA</p>
										</div>
										<ul class="property-feature">
											<li> <i class="fas fa-bed"></i>
												<span>4 Bedrooms</span>
											</li>
											<li> <i class="fas fa-bath"></i>
												<span>3 Bath</span>
											</li>
											<li> <i class="fas fa-arrows-alt"></i>
												<span>2142 sq ft</span>
											</li>
											<li> <i class="fas fa-car"></i>
												<span>2 Garage</span>
											</li>
										</ul>
										<div class="trending-bottom">
											<div class="trend-left float-left">
												<ul class="product-rating">
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star-half-alt"></i></li>
													<li><i class="fas fa-star-half-alt"></i></li>
												</ul>
											</div>
											<a class="trend-right float-right">
												<div class="trend-open">
													<p><span class="per_sale">starts from</span>$25000</p>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="single-property-box">
									<div class="property-item">
										<a class="property-img" href="single-listing-two.html"><img src="" alt="#"></a>
										<ul class="feature_text">
											<li class="feature_or"><span>For Rent</span></li>
										</ul>
										<div class="property-author-wrap">
											<a href="#" class="property-author">
												<img src="" alt="...">
												<span>Zilion Properties</span>
											</a>
											<ul class="save-btn">
												<li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
												<li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="property-title-box">
										<h4><a href="single-listing-one.html">Family home in Glasgow</a></h4>
										<div class="property-location">
											<i class="fa fa-map-marker-alt"></i>
											<p>60 High St, Glasgow, London</p>
										</div>
										<ul class="property-feature">
											<li> <i class="fas fa-bed"></i>
												<span>3 Bedrooms</span>
											</li>
											<li> <i class="fas fa-bath"></i>
												<span>3 Bath</span>
											</li>
											<li> <i class="fas fa-arrows-alt"></i>
												<span>1982 sq ft</span>
											</li>
											<li> <i class="fas fa-car"></i>
												<span>1 Garage</span>
											</li>
										</ul>
										<div class="trending-bottom">
											<div class="trend-left float-left">
												<ul class="product-rating">
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star-half-alt"></i></li>
													<li><i class="fas fa-star-half-alt"></i></li>
												</ul>
											</div>
											<a class="trend-right float-right">
												<div class="trend-open">
													<p>$7500<span class="per_month">month</span> </p>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="single-property-box">
									<div class="property-item">
										<a class="property-img" href="single-listing-two.html"><img src="" alt="#"> </a>
										<ul class="feature_text">
											<li class="feature_cb"><span>New</span></li>
											<li class="feature_or"><span>For Sale</span></li>
										</ul>
										<div class="property-author-wrap">
											<a href="#" class="property-author">
												<img src="" alt="...">
												<span>Hexa Properties</span>
											</a>
											<ul class="save-btn">
												<li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
												<li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="property-title-box">
										<h4><a href="single-listing-one.html">Office Space in Thatcham</a></h4>
										<div class="property-location">
											<i class="fa fa-map-marker-alt"></i>
											<p>Colthrop Lane, Thatcham, London</p>
										</div>
										<ul class="property-feature">
											<li> <i class="fas fa-home"></i>
												<span>6 Rooms</span>
											</li>
											<li> <i class="fas fa-bath"></i>
												<span>2 Bath</span>
											</li>
											<li> <i class="fas fa-arrows-alt"></i>
												<span>1400 sq ft</span>
											</li>
											<li> <i class="fas fa-car"></i>
												<span>1 Garage</span>
											</li>
										</ul>
										<div class="trending-bottom">
											<div class="trend-left float-left">
												<ul class="product-rating">
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star-half-alt"></i></li>
													<li><i class="fas fa-star-half-alt"></i></li>
												</ul>
											</div>
											<a class="trend-right float-right">
												<div class="trend-open">
													<p><span class="per_sale">starts from</span>$12000</p>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="single-property-box">
									<div class="property-item">
										<a class="property-img" href="single-listing-two.html"><img src="" alt="#"></a>
										<ul class="feature_text">
											<li class="feature_cb"><span> New</span></li>
											<li class="feature_or"><span>For Sale</span></li>
										</ul>
										<div class="property-author-wrap">
											<a href="#" class="property-author">
												<img src="" alt="...">
												<span>Bob Haris</span>
											</a>
											<ul class="save-btn">
												<li data-toggle="tooltip" data-placement="top" title="Video"><a href="https://www.youtube.com/watch?v=v_ATnE02qFs" class="property-yt"><i class="fas fa-play"></i></a></li>
												<li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
												<li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="property-title-box">
										<h4><a href="single-listing-one.html">Apartment in Cecil Lake</a></h4>
										<div class="property-location">
											<i class="fa fa-map-marker-alt"></i>
											<p>131 midlas , Cecil Lake, BC</p>
										</div>
										<ul class="property-feature">
											<li> <i class="fas fa-bed"></i>
												<span>3 Bedrooms</span>
											</li>
											<li> <i class="fas fa-bath"></i>
												<span>2 Bath</span>
											</li>
											<li> <i class="fas fa-arrows-alt"></i>
												<span>1600 sq ft</span>
											</li>
											<li> <i class="fas fa-car"></i>
												<span>1 Garage</span>
											</li>
										</ul>
										<div class="trending-bottom">
											<div class="trend-left float-left">
												<ul class="product-rating">
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star-half-alt"></i></li>
													<li><i class="fas fa-star-half-alt"></i></li>
												</ul>
											</div>
											<a class="trend-right float-right">
												<div class="trend-open">
													<p><span class="per_sale">starts from</span>$9000</p>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="single-property-box">
									<div class="property-item">
										<a class="property-img" href="single-listing-two.html"><img src="" alt="#"> </a>
										<ul class="feature_text">
											<li class="feature_cb"><span>New</span></li>
											<li class="feature_or"><span>For Rent</span></li>
										</ul>
										<div class="property-author-wrap">
											<a href="#" class="property-author">
												<img src="" alt="...">
												<span>Tony Stark</span>
											</a>
											<ul class="save-btn">
												<li data-toggle="tooltip" data-placement="top" title="Photos"><a href=".photo-gallery" class="btn-gallery"><i class="lnr lnr-camera"></i></a></li>
												<li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
												<li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
											</ul>
											<div class="hidden photo-gallery">
												<a href=""></a>
												<a href=""></a>
												<a href=""></a>
												<a href=""></a>
												<a href=""></a>
												<a href=""></a>
												<a href=""></a>
											</div>
										</div>
									</div>
									<div class="property-title-box">
										<h4><a href="single-listing-one.html">Comfortable Family Apartment</a></h4>
										<div class="property-location">
											<i class="fa fa-map-marker-alt"></i>
											<p>4210 Khale Street, Florence, USA</p>
										</div>
										<ul class="property-feature">
											<li> <i class="fas fa-bed"></i>
												<span>2 Bedrooms</span>
											</li>
											<li> <i class="fas fa-bath"></i>
												<span>2 Bath</span>
											</li>
											<li> <i class="fas fa-arrows-alt"></i>
												<span>1500 sq ft</span>
											</li>
											<li> <i class="fas fa-car"></i>
												<span>1 Garage</span>
											</li>
										</ul>
										<div class="trending-bottom">
											<div class="trend-left float-left">
												<ul class="product-rating">
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star"></i></li>
													<li><i class="fas fa-star-half-alt"></i></li>
													<li><i class="fas fa-star-half-alt"></i></li>
												</ul>
											</div>
											<a class="trend-right float-right">
												<div class="trend-open">
													<p>$7500<span class="per_month">month</span> </p>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="slider-btn v2 similar-next"><i class="lnr lnr-arrow-right"></i></div>
					<div class="slider-btn v2 similar-prev"><i class="lnr lnr-arrow-left"></i></div>
				</div> -->
			</div>
		</div>
	</div>
	<!--Similar Listing ends-->
</div>
</div>
<!--Page Wrapper ends-->
<!--Footer Starts-->
<div class="footer-wrapper v3">
	<div class="footer-top-area">
		<div class="container">
			<div class="row nav-folderized">
				<div class="col-lg-4 col-md-12">
					<div class="footer-logo">
						<a href="index.html"> <img src="" alt="logo"></a>
						<div class="company-desc">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi nam totam sequi, ipsam consequatur repudiandae libero illum.
							</p>
							<ul class="list footer-list mt-20">
								<li>
									<div class="contact-info">
										<div class="icon">
											<i class="fa fa-map-marker-alt"></i>
										</div>
										<div class="text">13th North Ave, Florida, USA</div>
									</div>
								</li>
								<li>
									<div class="contact-info">
										<div class="icon">
											<i class="fas fa-envelope"></i>
										</div>
										<div class="text"><a href="#">info@sarchholm.com</a></div>
									</div>
								</li>
								<li>
									<div class="contact-info">
										<div class="icon">
											<i class="fas fa-phone-alt"></i>
										</div>
										<div class="text">+44 078 767 595</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-12 text-center sm-left">
					<div class="footer-content nav">
						<h2 class="title">Popular Searches</h2>
						<ul class="list res-list">
							<li><a class="link-hov style2" href="#">Property for Rent</a></li>
							<li><a class="link-hov style2" href="#">Property for Sale</a></li>
							<li><a class="link-hov style2" href="#">Condominium for Sale</a></li>
							<li><a class="link-hov style2" href="#">Resale Properties</a></li>
							<li><a class="link-hov style2" href="#">Recent Properties</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-12 text-center sm-left">
					<div class="footer-content nav">
						<h2 class="title">Quick Links</h2>
						<ul class="list res-list">
							<li><a class="link-hov style2" href="about.html">About us</a></li>
							<li><a class="link-hov style2" href="contact.html">Contact us</a></li>
							<li><a class="link-hov style2" href="faq.html">Privacy Policy</a></li>
							<li><a class="link-hov style2" href="faq.html">FAQ</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-12">
					<div class="footer-content">
						<h4 class="title">Subscribe</h4>
						<div class="value-input-wrap newsletter">
							<form action="#">
								<input type="text" placeholder="Your mail address ..">
								<button type="submit">Subscribe</button>
							</form>
						</div>
						<div class="footer-social-wrap">
							<p>Follow us on </p>
							<ul class="social-buttons style2">
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
								<li><a href="#"><i class="fab fa-youtube"></i></a></li>
								<li><a href="#"><i class="fab fa-dribbble"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom-area">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-8 offset-md-2">
					<p>
					©SarchHolm 2019. All rights reserved. </p>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Footer ends-->
<!--login Modal starts-->
<div class="modal fade" id="user-login-popup">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="lnr lnr-cross"></i></span></button>
			</div>
			<div class="modal-body">
				<!--User Login section starts-->
				<div class="user-login-section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<ul class="ui-list nav nav-tabs mb-30" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" data-toggle="tab" href="#login" role="tab" aria-selected="true">Login</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" data-toggle="tab" href="#register" role="tab" aria-selected="false">Register</a>
									</li>
								</ul>
								<div class="login-wrapper">
									<div class="ui-dash tab-content">
										<div class="tab-pane fade show active" id="login" role="tabpanel">
											<div class="row">
												<div class="col-md-6">
													<form id="login-form" action="#" method="post">
														<div class="form-group">
															<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="email" value="" required="">
														</div>
														<div class="form-group">
															<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
														</div>
														<div class="row mt-20">
															<div class="col-md-6 col-12 text-left">
																<div class="res-box">
																	<input id="check-l" type="checkbox" name="check">
																	<label for="check-l">Remember me</label>
																</div>
															</div>
															<div class="col-md-6 col-12 text-right">
																<div class="res-box sm-left">
																	<a href="#" tabindex="5" class="forgot-password">Forgot Password?</a>
																</div>
															</div>
														</div>
														<div class="res-box text-center mt-30">
															<button type="submit" class="btn v8"><span class="lnr lnr-exit"></span> Log In</button>
														</div>
													</form>
													<div class="social-profile-login  text-left mt-20">
														<h5>or Login with</h5>
														<ul class="social-btn">
															<li class="bg-fb"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
															<li class="bg-tt"><a href="#"><i class="fab fa-twitter"></i></a></li>
															<li class="bg-ig"><a href="#"><i class="fab fa-instagram"></i></a></li>
														</ul>
													</div>
												</div>
												<div class="col-md-6">
													<div class="signup-wrapper">
														<img src="" alt="...">
														<p>Welcome Back! Please Login to your Account for latest property listings.</p>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="register" role="tabpanel">
											<div class="row">
												<div class="col-md-6">
													<form id="register-form" action="#" method="post">
														<div class="form-group">
															<input type="text" name="user_name" id="user_name" tabindex="1" class="form-control" placeholder="Username" value="">
														</div>
														<div class="form-group">
															<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="">
														</div>
														<div class="form-group">
															<input type="password" name="user_password" id="user_password" tabindex="2" class="form-control" placeholder="Password">
														</div>
														<div class="form-group">
															<input type="password" name="confirm-password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password">
														</div>
														<div class="res-box text-left">
															<input type="checkbox" tabindex="3" class="" name="remember" id="remember">
															<label for="remember">I've read and accept terms &amp; conditions</label>
														</div>
														<div class="res-box text-center mt-30">
															<button type="submit" class="btn v8"><i class="lnr lnr-enter"></i>Sign Up</button>
														</div>
													</form>
													<div class="social-profile-login  text-left mt-20">
														<h5>or Sign Up with</h5>
														<ul class="social-btn">
															<li class="bg-fb"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
															<li class="bg-tt"><a href="#"><i class="fab fa-twitter"></i></a></li>
															<li class="bg-ig"><a href="#"><i class="fab fa-instagram"></i></a></li>
														</ul>
													</div>
												</div>
												<div class="col-md-6">
													<div class="signup-wrapper">
														<img src="" alt="...">
														<p>Create an account to find the best Property for you.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--User login section ends-->
			</div>
		</div>
	</div>
</div>
<!--login Modal ends-->
<!--Scripts starts-->
<!--plugin js-->
<script src="{{ asset('/single-two-js/plugin.js') }}"></script>
<script src="{{ asset('/single-two-js/main.js') }}"></script>

<!--google maps-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_8C7p0Ws2gUu7wo0b6pK9Qu7LuzX2iWY&amp;libraries=places&amp;callback=initAutocomplete"></script>
<!--Main js-->

<script type="text/javascript">
	$(document).ready(function(){
		$("input#submit_enquiry").on('click',function(e){
			e.preventDefault();

                
                var csrf = $("input#enquiry_csrf").val();
                var cust_name = $("input#cust_name").val();
                var email = $("input#cust_email").val();
                var phone = $("input#cust_phone").val();
                var comment = $("textarea#comment").val();
                

                if(cust_name == '' || cust_name.length == 0)
                {
                    if($("input#cust_name").siblings("span").length == 0)
                    {
                        $("input#cust_name").parent("div").append("<span style='color:red;'>Name required.</span>");
                    }
                    
                    
                    return false;
                }
                else
                {
                    if($("input#cust_name").siblings("span").length > 0)
                    {
                        $("input#cust_name").siblings("span")[0].remove();
                    }

                }

                /*if(email == '' || email.length == 0)
                {
                    
                    if($("input#email").siblings("span").length == 0)
                    {
                        $("input#email").parent("div.form-group").append("<span style='color:red;'>Email required.</span>");
                    }
                    else 
                                        
                    return false;
                }
                else
                {
                    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                    
                        
                    if(!email.match(mailformat))
                    {
                        $("input#email").parent("div.form-group").append("<span style='color:red;'>Invalid Email.</span>");
                        return false;
                    }
                    else if($("input#email").siblings("span").length > 0)
                    {

                        $("input#email").siblings("span")[0].remove();
                    }
                }*/

                

                if(phone == '' || phone.length == 0)
                {
                    if($("input#cust_phone").siblings("span").length == 0)
                    {
                        $("input#cust_phone").parent("div").append("<span style='color:red;'>Phone number required.</span>");
                    }
                                        
                    return false;
                }
                else
                {
                    if($("input#cust_phone").siblings("span").length > 0)
                    {
                        $("input#cust_phone").siblings("span")[0].remove();
                    }
                }

                

                var postdata = {'_token' : csrf, 'cust_name' : cust_name,
                                'cust_email' : email,  'cust_phone' : phone, 'comment':comment};
                $.ajax({
                    type:'POST',
                    url : "{{ url('/property/enquiry') }}/{{$prop_details->property_id}}",
                    data : postdata,
                    success : function(res)
                    {
                        var post_res = JSON.parse(res);
                        /*if(post_res.status)
                        {
                            $("div#reg-status").html("");
                            $("div#reg-status").html("<h4 style='color:green;text-align:center;'>"+post_res.message+"</h4>");
                            $("div#reg-status").slideDown(function() {
                                setTimeout(function() {
                                    $("div#reg-status").slideUp();
                                }, 5000);
                            });

                            $("input#user_name").val("");
                            $("input#email").val("");
                            $("input#user_password").val("");
                            $("input#confirm-password").val("");
                            $("input#phone").val("");
                        }
                        else
                        {
                           $("div#reg-status").html("");
                            $("div#reg-status").html("<h4 style='color:red;text-align:center;'>"+post_res.message+"</h4>");
                            $("div#reg-status").slideDown(function() {
                                setTimeout(function() {
                                    $("div#reg-status").slideUp();
                                }, 5000);
                            });
                        }*/
                    console.log(post_res)
                     $("input#cust_name").val("");
                     $("input#cust_phone").val("");
                     $("input#cust_email").val("");
                     $("input#comment").val("");
                    }
                })
            
		})
	});
</script>

<!--Scripts ends-->
</body>

</html>
