@extends('layouts.layout')



@section('head')



@endsection
@section('content')

    {{--    @include('frontend.includes.frontheader')--}}

    <div class="filter-wrapper style1">
        <div class="container bg-add-listing">
            <div class="row">
                <div class="col-xl-3 col-lg-12">

                    @include('frontend.user.left_menu')

                </div>
                <!--Sidebar ends-->
                <div class="col-xl-9 col-lg-12">

                    <div class="sidebar-content-right py-100">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">


                                <ul class="nav nav-tabs list-details-tab">
                                    <li class="nav-item {{ (!isset($active_opt) || $active_opt=='') ? ' active ':'' }} ">
                                        <a class=" nav-link " data-toggle="tab" href="#general_info">Basic Info</a>
                                    </li>
                                    <li class="nav-item {{ (isset($active_opt) && $active_opt=='location' ) ? ' active ':'' }} "
                                        id="location-item">
                                        <a class=" nav-link {{ (isset($active_opt) &&  in_array($active_opt, array('location','property_info','amenties','upload_photos','packages'))) ? '  ':'disabled' }} "
                                           data-toggle="tab" href="#location">
                                            Location
                                        </a>
                                    </li>
                                    <li class="nav-item {{ (isset($active_opt) && $active_opt=='property_info' ) ? ' active ':'' }}"
                                        id="property-info">
                                        <a class="nav-link {{ (isset($active_opt) && in_array($active_opt, array('property_info','amenties','upload_photos', 'packages')) ) ? '  ':'disabled' }}"
                                           data-toggle="tab" href="#property_info">
                                            Property Info
                                        </a>
                                    </li>
                                    <li class="nav-item {{ (isset($active_opt) && $active_opt=='amenties' ) ? ' active ':'' }}"
                                        id="amenities">
                                        <a class="nav-link {{ (isset($active_opt) && in_array($active_opt, array('amenties', 'upload_photos', 'packages')) ) ? '  ':'disabled' }}"
                                           data-toggle="tab" href="#amenties">
                                            Amenities
                                        </a>
                                    </li>
                                    <li class="nav-item {{ (isset($active_opt) && $active_opt=='upload_photos' ) ? ' active ':'' }}"
                                        id="upload-photos">
                                        <a class="nav-link {{ (isset($active_opt) && in_array($active_opt, array('upload_photos','packages')) ) ? '  ':'disabled' }}"
                                           data-toggle="tab" href="#upload_photos">
                                            Upload Photos
                                        </a>
                                    </li>
                                    <li class="nav-item" id="prop-packages">
                                        <a class="nav-link {{ (isset($active_opt) && in_array($active_opt, array('packages')) ) ? '  ':'disabled' }}"
                                           data-toggle="tab" href="#packages">Packages</a>
                                    </li>
                                </ul>

                                @csrf
                                <div class="tab-content my-30 add_list_content col-lg-8">

                                    {{ $active_opt }}

                                    <?php
                                    dump($property_info);
                                    ?>

                                    <div class="tab-pane {{ (!isset($active_opt) || $active_opt=='') ? ' active ':'' }}  "
                                         id="general_info">
                                        <form id="post_prop_form_general_info"
                                              action="{{ route('ajax.user.property.manage_process', ['id'=>$property_info->id]) }}"
                                              method="post"
                                              enctype="multipart/form-data">

                                            @csrf

                                            <input type="hidden" name="post_actions" value="general_info"/>
                                            <input type="hidden" name="current_property_id"
                                                   value="{{ $property_info->id }}"/>
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group" id="you_r">
                                                        <label>You are</label>
                                                        <div class="input-group listing_radio">

                                                            <?php
                                                            //                                                            dump($user_types)
                                                            ?>

                                                            @foreach ($user_types as $i => $ut)

                                                                <input name="user_type_id" type="radio"
                                                                       value="{{$ut->id}}"
                                                                       {{ $property_info->user_type_id==$ut->id ? 'checked':'' }}
                                                                       id="choice_{{$i}}" tabindex="1">
                                                                <label for="choice_{{$i}}">{{ $ut->user_type }}</label>
                                                            @endforeach


                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group" id="about_you">
                                                        <label>Tell about yourself</label>

                                                        <textarea name="about_you" class="form-control" id="about_you"
                                                                  rows="4"
                                                                  placeholder="">{{ $property_info->about_you }}</textarea>

                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group" id="i_want">
                                                        <label>I want</label>
                                                        <div class="input-group  listing_radio">
                                                            @foreach($property_uses as $i => $pu)

                                                                <input name="use_type_id" type="radio"
                                                                       value="{{$pu->id}}" id="use_type_id_{{$i}}"
                                                                       {{ $property_info->use_type_id==$pu->id ? 'checked':'' }}
                                                                       tabindex="1">
                                                                <label for="use_type_id_{{$i}}">{{ $pu->use_type }}</label>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-md-12">
                                                    <div class="form-group" id="prop_type">
                                                        <label>Property Type</label>
                                                        <div class="input-group  listing_radio">

                                                            <?php
                                                            //                                                            dd($property_types);
                                                            ?>

                                                            @foreach($property_types  as $i =>  $pt)

                                                                <input name="property_type_id" type="radio"
                                                                       class="property_type_id"
                                                                       data-name="{{$pt->property_type}}"
                                                                       {{ $property_info->property_type_id==$pt->id ? 'checked':'' }}
                                                                       value="{{$pt->id}}"
                                                                       id="property_type_id_{{$i}}" tabindex="1">
                                                                <label for="property_type_id_{{$i}}">{{$pt->property_type}}</label>
                                                            @endforeach
                                                        </div>


                                                    </div>
                                                </div>

                                                <div class="col-md-12" id="property_type_sub1" type="">

                                                </div>


                                                <div class="col-md-12">
                                                    <div class="form-group" id="avail">
                                                        <label>Available</label>
                                                        <div class="input-group  listing_radio">
                                                            @foreach($availability as $i => $av)

                                                                <input name="availability_id" type="radio"
                                                                       value="{{$av->id}}"
                                                                       {{ $property_info->availability_id==$av->id ? 'checked':'' }}
                                                                       id="availability_id_{{$i}}" tabindex="1">
                                                                <label for="availability_id_{{$i}}">{{$av->available_for}}</label>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group possessions" id="possessions">
                                                        <label>Possession</label>
                                                        <div class="input-group  listing_radio">
                                                            @foreach($possessions as $i => $poss)

                                                                <input name="possession_id" type="radio"
                                                                       class="possessions_input"
                                                                       {{ $property_info->possession_id== $poss->id ? 'checked':'' }}
                                                                       value="{{$poss->id}}" id="possession_id_{{$i}}"
                                                                       tabindex="1">
                                                                <label for="possession_id_{{$i}}">{{$poss->possession}}</label>
                                                            @endforeach
                                                        </div>

                                                        <div class="col-md-6" id="possession_calender">


                                                        </div>


                                                        {{--                                                        <input name="asdas" class=" datepicker_ui" id="datepicker_ui" type="text" placeholder="Date"/>--}}
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="add-btn">
                                                <button type="submit" class="btn v3">CONTINUE</button>
                                                {{--                                                <a id="basic_cont_btn" href="#" class="btn v3">CONTINUE</a>--}}
                                            </div>
                                        </form>
                                    </div>

                                    <div class="tab-pane {{ (isset($active_opt) && $active_opt=='location') ? 'active ':'' }}"
                                         id="location">

                                        <form id="setp2_locations_Valiodation"
                                              action="{{ route('ajax.user.property.manage_process', ['id'=>$property_info->id]) }}"
                                              method="post"
                                              enctype="multipart/form-data">

                                            <input type="hidden" name="post_actions" value="location"/>
                                            <input type="hidden" name="current_property_id"
                                                   value="{{ $property_info->id }}"/>

                                            @csrf

                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Locality</label>
                                                        <div class="input-group">
                                                            <input name="locality" type="text" id="autocomplete"
                                                                   class="form-control filter-input"
                                                                   value="{{ $property_info->locality }}"
                                                                   placeholder="eg: street, city, area name">
                                                            {{--                                                            or <a href="" class="highlight_text">Choose from map</a>--}}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group" id="state_id">
                                                        <label>State</label>
                                                        <div class="input-group">
                                                            <select name="state_id" id="state_list"
                                                                    class="listing-input hero__form-input  custom-select">
                                                                <option value="">Select State</option>
                                                                @foreach($states as $i => $state)
                                                                    <option value="{{$state->id}}"
                                                                            selected {{ $property_info->state_id==$state->id }}>{{$state->state}}</option>

                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group" id="city_id">
                                                        <label>City</label>
                                                        <div class="input-group">
                                                            <input name="city_id" type="text" id="locality"
                                                                   autocomplete="off"
                                                                   value="{{ $property_info->city_id }}"
                                                                   class="form-control filter-input"
                                                                   placeholder="Enter City Name">
                                                            {{--                                                        <select name="city_id" id="city_list"--}}
                                                            {{--                                                                class="listing-input hero__form-input  custom-select">--}}
                                                            {{--                                                            <option value="">Select City</option>--}}

                                                            {{--                                                        </select>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Project Name (Optional)</label>
                                                        <div class="input-group">
                                                            <input name="project_name" type="text"
                                                                   value="{{ $property_info->project_name }}"
                                                                   class="form-control filter-input" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group" id="prop_addrs">
                                                        <label>Address</label>
                                                        <div class="input-group">
                                                            <input name="property_address" type="text"
                                                                   class="form-control filter-input"
                                                                   value="{{ $property_info->property_address }}"
                                                                   placeholder="Enter Your Property Address">
                                                        </div>
                                                    </div>
                                                    <!-- <input id="pac-input" class="controls" type="text" placeholder="Search Box"> -->
                                                    <!-- <div id="map"></div> -->
                                                </div>
                                                <div class="add-btn">
                                                    <button type="submit" class="btn v3">CONTINUE</button>
                                                    {{--                                                    <a href="#" id="location-cont-btn" class="btn v3">CONTINUE</a>--}}
                                                </div>
                                                <!-- <div class="col-md-12">
                                                    <div id="map" class="form-group">
                                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d118931.82141015177!2d78.41900087600888!3d17.36914750298335!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb99daeaebd2c7%3A0xae93b78392bafbc2!2sHyderabad%2C%20Telangana%2C%20India!5e0!3m2!1sen!2ssg!4v1577368654736!5m2!1sen!2ssg"
                                                        width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                                    </div>
                                                </div> -->
                                            </div>

                                        </form>

                                    </div>
                                    <div class="tab-pane {{ (isset($active_opt) && $active_opt=='property_info') ? 'active ':'' }}"
                                         id="property_info">
                                        <div class="row">
                                            <div class="col-md-6" id="plot_area">
                                                <div class="form-group">
                                                    <label>Plot Area</label>
                                                    <input type="number" name="plot_area"
                                                           class="form-control filter-input" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" id="plot_area_measure">
                                                    <label>Select</label>
                                                    <select name="plot_area_measure_id"
                                                            class="listing-input hero__form-input  custom-select">
                                                        @foreach($measures as $i => $measure)
                                                            <option value="{{$measure['measure_id']}}">{{$measure['measure']}}</option>

                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Plots floor allow for construction: (Optional)</label>
                                                    <select name="no_of_plots_per_floor"
                                                            class="listing-input hero__form-input  custom-select">
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5+</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Length (Optional)</label>
                                                    <input type="number" name="length"
                                                           class="form-control filter-input" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Select</label>
                                                    <select name="length_measure_id"
                                                            class="listing-input hero__form-input  custom-select">
                                                        @foreach($measures as $i => $measure)
                                                            <option value="{{$measure['measure_id']}}">{{$measure['measure']}}</option>

                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Width (Optional)</label>
                                                    <input name="width" type="text"
                                                           class="form-control filter-input" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Select</label>
                                                    <select name="width_measure_id"
                                                            class="listing-input hero__form-input  custom-select">
                                                        @foreach($measures as $i => $measure)
                                                            <option value="{{$measure['measure_id']}}">{{$measure['measure']}}</option>

                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" id="width_of_facing">
                                                    <label>Width of facing</label>
                                                    <input name="width_of_facing" type="number"
                                                           class="form-control filter-input" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Select</label>
                                                    <select name="facing_measure_id"
                                                            class="listing-input hero__form-input  custom-select">
                                                        @foreach($measures as $i => $measure)
                                                            <option value="{{$measure['measure_id']}}">{{$measure['measure']}}</option>

                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group" id="plot_price">
                                                    <label>Plot Price</label>
                                                    <input name="plot_price" type="number"
                                                           class="form-control filter-input" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group" id="price_per_sq_feet">
                                                    <label>Price per sq.feet</label>
                                                    <input name="price_per_sq_feet" type="number"
                                                           class="form-control filter-input" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Select</label>
                                                    <div class="filter-checkbox">
                                                        <input id="negotiable" type="checkbox" name="negotiable"
                                                               value="1">
                                                        <label for="negotiable">Negotiable</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Plot Booking Amount</label>
                                                    <input name="plot_booking_amount" type="number"
                                                           class="form-control filter-input" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Maintenance Amount</label>
                                                    <input type="number" name="maintenance_amount"
                                                           class="form-control filter-input" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Select</label>
                                                    <select name="booking_type"
                                                            class="listing-input hero__form-input  custom-select">
                                                        <option value="month">Month</option>
                                                        <option value="year">Year</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">

                                                <div class="form-group">
                                                    <label for="list_info">Write about Property (50-1000) </label>
                                                    <textarea name="about_property" class="form-control"
                                                              id="list_info" rows="4"
                                                              placeholder="Enter your text here"></textarea>
                                                </div>

                                            </div>
                                            <div class="add-btn">
                                                <a id="prop_info_cont_btn" href="#" class="btn v3">CONTINUE</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane {{ (isset($active_opt) && $active_opt=='amenties') ? 'active ':'' }}"
                                         id="amenties">
                                        <div class="row mb-30">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Amenities</label>
                                                    <div id="amenities" class="filter-checkbox">
                                                        <input id="check-a" type="checkbox" name="amenities[]"
                                                               value="basketball">
                                                        <label for="check-a">Basketball court</label>
                                                        <input id="check-b" type="checkbox" name="amenities[]"
                                                               value="gym">
                                                        <label for="check-b">Gym</label>
                                                        <input id="check-c" type="checkbox" name="amenities[]"
                                                               value="washer">
                                                        <label for="check-c">washer and dryer </label>
                                                        <input id="check-d" type="checkbox" name="amenities[]"
                                                               value="wheelchair">
                                                        <label for="check-d">Wheelchair Friendly</label>

                                                        <input id="check-e" type="checkbox" name="amenities[]"
                                                               value="aircondition">
                                                        <label for="check-e">Air Conditioned</label>
                                                        <input id="check-z" type="checkbox" name="amenities[]"
                                                               value="swimmingpool">
                                                        <label for="check-z">Swimming Pool</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="add-btn">
                                                <a href="#" class="btn v3" id="amenities_btn">CONTINUE</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane {{ (isset($active_opt) && $active_opt=='upload_photos') ? 'active ':'' }}"
                                         id="upload_photos">
                                        <div class="row">
                                            <div class="col-md-12">


                                                <div class="input-field">
                                                    <label class="active">Photos</label>
                                                    <div class="input-images-1" style="padding-top: .5rem;"></div>
                                                </div>


                                            </div>
                                            <div class="col-md-12 mt-30">
                                                <label>Property Video Link</label>
                                                <input name="video_url" type="text"
                                                       class="form-control filter-input"
                                                       placeholder="eg: https://www.youtube.com/watch?v=dqD0SqMNtks">
                                                <div class="add-btn">
                                                    <a href="#" class="btn v3" id="packages_btn">CONTINUE</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane {{ (isset($active_opt) && $active_opt=='packages') ? 'active ':'' }}"
                                         id="packages">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group" id="">

                                                    <div class="listing_radio">

                                                        <input name="package" type="radio" value="free"
                                                               id="package_1" tabindex="1">
                                                        <label for="package_1">Free</label>

                                                    </div>
                                                    <br>
                                                    <div class="filter-checkbox">
                                                        <input type="checkbox" tabindex="1" class="" name="terms"
                                                               id="terms">
                                                        <label for="terms">I Agreed Terms and Conditions</label>

                                                    </div>
                                                    <div class="listing_radio">

                                                        <input name="package" type="radio" value="silver"
                                                               id="package_2" tabindex="2">
                                                        <label for="package_2">Silver ( 2000 )</label>
                                                        <input name="package" type="radio" value="silver"
                                                               id="package_3" tabindex="3">
                                                        <label for="package_3">Gold ( 5000 )</label>
                                                        <input name="package" type="radio" value="silver"
                                                               id="package_4" tabindex="4">
                                                        <label for="package_4">Platinum ( 10,000 )</label>

                                                    </div>


                                                </div>
                                            </div>
                                            <div class="col-md-12 mt-30">

                                                <div class="add-btn">
                                                    <!-- <a href="#" class="btn v3 mt-30">POST PROPERTY</a> -->
                                                    <button class="btn v3" type="submit">POST PROPERTY</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_script')



    {{--    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGvnjb9So7e9PrYfxqUbAUD4GVTV4JwqA&libraries=places&callback=initAutocomplete" async defer></script>--}}

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-PT0RzsNRhTT9fLDRcxngGbE4TPB5uMM&libraries=places&language=en"></script>

    <script>


        var input = document.getElementById('autocomplete');
        var autocomplete = new google.maps.places.Autocomplete(input);


        var componentForm = {
            // street_number: 'short_name',
            // route: 'long_name',
            locality: 'long_name',
            // administrative_area_level_1: 'short_name',
            // administrative_area_level_2: 'short_name',
            // country: 'long_name',
            // postal_code: 'short_name'
        };


        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            console.log(place);

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details,
            // and then fill-in the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        }

        autocomplete.addListener('place_changed', fillInAddress);


    </script>



    @include('frontend.user.property.add_property_script')
@endsection

