@extends('layouts.layout')



@section('head')



@endsection
@section('content')

    {{--    @include('frontend.includes.frontheader')--}}

    <div class="filter-wrapper style1">
        <div class="container bg-add-listing">
            <div class="row">
                <div class="col-xl-3 col-lg-12">

                    @include('frontend.user.left_menu')

                </div>
                <!--Sidebar ends-->
                <div class="col-xl-9 col-lg-12">

                    <div class="sidebar-content-right py-100">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">


                                <ul class="nav nav-tabs list-details-tab">
                                    <li class="nav-item {{ (!isset($active_opt) || $active_opt=='') ? ' active ':'' }} ">
                                        <a class=" nav-link " data-toggle="tab" href="#general_info">Basic Info</a>
                                    </li>
                                    <li class="nav-item {{ (isset($active_opt) && $active_opt=='location' ) ? ' active ':'' }} " id="location-item">
                                        <a class=" nav-link {{ (isset($active_opt) && $active_opt=='location') ? '  ':'disabled' }} "
                                           data-toggle="tab" href="#location">Location</a>
                                    </li>
                                    <li class="nav-item" id="property-info">
                                        <a class="nav-link disabled" data-toggle="tab" href="#property_info">Property
                                            Info</a>
                                    </li>
                                    <li class="nav-item" id="amenities">
                                        <a class="nav-link disabled" data-toggle="tab" href="#amenties">Amenities</a>
                                    </li>
                                    <li class="nav-item" id="upload-photos">
                                        <a class="nav-link disabled" data-toggle="tab" href="#upload_photos">Upload
                                            Photos</a>
                                    </li>
                                    <li class="nav-item" id="prop-packages">
                                        <a class="nav-link disabled" data-toggle="tab" href="#packages">Packages</a>
                                    </li>
                                </ul>

                                @csrf
                                <div class="tab-content my-30 add_list_content col-lg-8">

{{--                                    {{ $active_opt }}--}}

                                    <div class="tab-pane active"   id="general_info">
                                        <form id="post_prop_form_general_info"
                                              action="{{ route('ajax.user.property.store') }}"
                                              method="post"
                                              enctype="multipart/form-data">

                                            @csrf

                                            <input type="hidden" name="post_actions" value="general_info"/>
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group" id="you_r">
                                                        <label>You are</label>
                                                        <div class="input-group listing_radio">

                                                            <?php
                                                            //                                                            dump($user_types)
                                                            ?>

                                                            @foreach ($user_types as $i => $ut)

                                                                <input name="user_type_id" type="radio"
                                                                       value="{{$ut->id}}"
                                                                       id="choice_{{$i}}" tabindex="1">
                                                                <label for="choice_{{$i}}">{{ $ut->user_type }}</label>
                                                            @endforeach


                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group" id="about_you">
                                                        <label>Tell about yourself</label>

                                                        <textarea name="about_you" class="form-control" id="about_you"
                                                                  rows="4" placeholder=""></textarea>

                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group" id="i_want">
                                                        <label>I want</label>
                                                        <div class="input-group  listing_radio">
                                                            @foreach($property_uses as $i => $pu)

                                                                <input name="use_type_id" type="radio"
                                                                       value="{{$pu->id}}" id="use_type_id_{{$i}}"
                                                                       tabindex="1">
                                                                <label for="use_type_id_{{$i}}">{{ $pu->use_type }}</label>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-md-12">
                                                    <div class="form-group" id="prop_type">
                                                        <label>Property Type</label>
                                                        <div class="input-group  listing_radio">

                                                            <?php
                                                            //                                                            dd($property_types);
                                                            ?>

                                                            @foreach($property_types  as $i =>  $pt)

                                                                <input name="property_type_id" type="radio"
                                                                       class="property_type_id"
                                                                       data-name="{{$pt->property_type}}"
                                                                       value="{{$pt->id}}"
                                                                       id="property_type_id_{{$i}}" tabindex="1">
                                                                <label for="property_type_id_{{$i}}">{{$pt->property_type}}</label>
                                                            @endforeach
                                                        </div>


                                                    </div>
                                                </div>

                                                <div class="col-md-12" id="property_type_sub1" type="">

                                                </div>


                                                <div class="col-md-12">
                                                    <div class="form-group" id="avail">
                                                        <label>Available</label>
                                                        <div class="input-group  listing_radio">
                                                            @foreach($availability as $i => $av)

                                                                <input name="availability_id" type="radio"
                                                                       value="{{$av->id}}"
                                                                       id="availability_id_{{$i}}" tabindex="1">
                                                                <label for="availability_id_{{$i}}">{{$av->available_for}}</label>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group possessions" id="possessions">
                                                        <label>Possession</label>
                                                        <div class="input-group  listing_radio">
                                                            @foreach($possessions as $i => $poss)

                                                                <input name="possession_id" type="radio"
                                                                       class="possessions_input"
                                                                       value="{{$poss->id}}" id="possession_id_{{$i}}"
                                                                       tabindex="1">
                                                                <label for="possession_id_{{$i}}">{{$poss->possession}}</label>
                                                            @endforeach
                                                        </div>

                                                        <div class="col-md-6" id="possession_calender">


                                                        </div>


                                                        {{--                                                        <input name="asdas" class=" datepicker_ui" id="datepicker_ui" type="text" placeholder="Date"/>--}}
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="add-btn">
                                                <button type="submit" class="btn v3">CONTINUE</button>
                                                {{--                                                <a id="basic_cont_btn" href="#" class="btn v3">CONTINUE</a>--}}
                                            </div>
                                        </form>
                                    </div>




                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('footer_script')



    {{--    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGvnjb9So7e9PrYfxqUbAUD4GVTV4JwqA&libraries=places&callback=initAutocomplete" async defer></script>--}}
    {{--    <script src="/modules/jquery.steps/jquery.steps.js" async defer></script>--}}

    {{--    <script src="/module/jquery.steps/jquery.steps.js"></script>--}}


    @include('frontend.user.property.add_property_script')
@endsection

