<?php

namespace App\Http\Middleware;

use Closure;

class CheckFrontUserSession
{

    public function handle($request, Closure $next)
    {
        if (!$request->session()->exists('username')) {
            // user value cannot be found in session
            return redirect(config('app.url'));
        }

        return $next($request);
    }

}