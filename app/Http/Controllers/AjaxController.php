<?php

namespace App\Http\Controllers;

use App\Models\PropertySubType;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Illuminate\Support\Facades\Auth;


class AjaxController extends Controller
{
    //
    public function propertyOpts(Request $request)
    {

        $requestData = $request->all();

        switch ($requestData['action']) {
            case "property_type_sub1":

                $property_sub_types = PropertySubType::where('property_type_id', $requestData['id'])->get();
//                dump($property_sub_types);

                if ($property_sub_types->count() > 0) {
                    ?>
                    <div id="Residential" class="form-group">
                        <hr>
                        <div class="input-group  listing_radio">
                            <?php
                            foreach ($property_sub_types as $i => $pst) {

                                ?>

                                <input name="property_sub_type_id" type="radio"
                                       value="<?= $pst->id ?>" <?= $requestData['active_id']==$pst->id ? ' checked ':' ' ?>
                                       id="property_sub_type_id_<?= $i ?>" tabindex="1">
                                <label for="property_sub_type_id_<?= $i ?>"><?= $pst->property_sub_type ?></label>
                                <?php
                            }
                            ?>
                        </div>
                        <hr>
                    </div>
                    <?php
                }


                break;
            case
            "property_type":
                break;
            case "possessions_date":

                if ($requestData['id'] == 2) {
                    ?>
                    <div class="form-group ">
                        <div class="input-group date">
                            <input name="possession_date"
                                   class="form-control datepicker_ui" autocomplete="off"
                                   type="text" value="<?= isset($requestData['possession_date']) ? date('d-m-Y', strtotime($requestData['possession_date'])) : '' ?>"
                                   placeholder="Date"/>
                            <span class="input-group-addon">
                            <i class="lnr lnr-calendar-full"></i>
                        </span>
                        </div>
                    </div>

                    <script>
                        datepickeUi();
                    </script>
                    <?php
                }


                break;
//            case "":
//                break;
        }


    }

    public
    function getCitiesOfState($state_id)
    {
        $cities = DB::table("cities")
            ->where('state_id', $state_id)
            ->get();
        if (count($cities->all()) > 0) {
            $cities = $cities->all();
            $res = array('status' => true, 'cities' => $cities);
            echo json_encode($res);
        } else {
            echo json_encode(array('status' => false, 'message' => "empty"));
        }

    }


    public
    function registerAjax(Request $request)
    {
        $returnData = array();
        $input = $request->all();


//        dd($input);

        $input['phone_number'] = str_replace("+91", "", $input['phone']);
        unset($input['phone']);
        $user = User::where('email', $input['email'])
            ->where('phone_number', $input['phone_number'])
            ->first();
        if (!empty($user)) {

            $returnData = array('status' => false, 'code' => 'user_exist', 'message' => 'User already registered.');


        } else {

            $inserData = array();

            $otpNumber = generateNumericOTP(6);


            $inserData['password'] = password_hash($input['user_password'], PASSWORD_DEFAULT);
            $inserData['is_admin'] = 0;
            $inserData['email'] = $input['email'];
            $inserData['phone_number'] = $input['phone_number'];
            $inserData['name'] = $input['user_name'];
//            $inserData['varify_otp'] = $otpNumber;

            $userData = User::create($inserData);

            $userData->varify_otp = $otpNumber;
            $userData->save();

            if (!empty($userData)) {


                $variables = array(
                    '{USERNAME}' => $userData->username,
                    '{OTP}' => $otpNumber
                );

                $MSG_USER_REGISTER_NOTFICATION_USER_OTP = strtr(MSG_USER_REGISTER_NOTFICATION_USER_OTP, $variables);

//                dd($MSG_USER_REGISTER_NOTFICATION_USER_OTP);


//                sendSms($userData->phone_number, $MSG_USER_REGISTER_NOTFICATION_USER_OTP);

                $returnData = array('status' => true, 'user' => $userData, 'code' => 'user_rigister', 'message' => 'User registered successfully');
            } else {
                $returnData = array('status' => false, 'user' => null, 'code' => 'user_data_error', 'message' => 'User registered error');
            }

        }

        return $returnData;
    }


    public
    function registerOtpVarify_Ajax(Request $request)
    {
        $returnData = array();
        $input = $request->all();


        $user = User::where('id', $input['user_id'])
            ->where('status', 0)
            ->first();
        if (!empty($user)) {

            $requestOtp = $input['otp'];

            if ($requestOtp == $user->varify_otp) {

                $updateUser = array(
                    'mobile_verified_at' => Carbon::now(),
                    'status' => 1,
                );

                $user->update($updateUser);


                $returnData = array('status' => true, 'code' => 'user_exist', 'message' => 'User Otp Varifyed..');


                $loginData = Auth::loginUsingId($user->id);

//              dd($loginData);


            } else {
                $returnData = array('status' => false, 'code' => 'invalid_otp', 'message' => 'Otp Invalid Otp');
            }

        } else {

            $returnData = array('status' => false, 'user' => null, 'code' => 'user_data_error', 'message' => 'User not avalable');
        }

        return $returnData;
    }


    public
    function loginAjax(Request $request)
    {
        $input = $request->all();


//        dd($input);

        $rules = array(
            'email' => 'required',
            'password' => 'required'
        );
        $userdata = $request->validate($rules);


        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, $request->has('remember'))) {


            $returnData = array('status' => true, 'user' => null, 'code' => 'user_set', 'message' => 'User Login');
        } else {
            $returnData = array('status' => false, 'user' => null, 'code' => 'user_data_error', 'message' => 'Invalid User Login');
        }

        if ($request->ajax()) {
            return $returnData;
        } else {
//            return redirect()->intended(URL::route('dashboard'));
        }


//        // run the validation rules on the inputs from the form
//        //$userdata = $request->validate($rules);
//        // create our user data for the authentication
//        DB::enableQueryLog();
//        $user = DB::table('users')
//            // ->Where(function ($query) {
//            /* In the email input field user can give phone number or email address. */
//            // $query
//            ->where('email', $userdata['username'])
//            ->orwhere('phone_number', $userdata['username'])
//            //->where('is_admin', 0)
//            // })
//            ->get();
//
//
//        if (count($user->all()) > 0) {
//            $user_details = $user->all()[0];
//
//            if (password_verify($userdata['password'], $user_details->password) && $user_details->is_admin == 0) {
//                Session::put('username', $user_details->username);
//                Session::put('user_id', $user_details->id);
//                echo json_encode(array('status' => true, 'message' => 'Invalid Credentials'));
//            } else {
//                echo json_encode(array('status' => false, 'message' => 'Invalid Credentials'));
//            }
//        } else {
//
//            echo json_encode(array('status' => false, 'message' => 'Invalid Credentials'));
//        }
    }

}
