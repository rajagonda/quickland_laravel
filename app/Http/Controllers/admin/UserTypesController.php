<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserType;
use View;

class UserTypesController extends Controller
{

    public function create()
    {
        return view('admin.pages.add_usertype');
    }
    public function listUserTypes()
    {

        $listOfUserTypes = UserType::all();
        $user_types = [];
        foreach ($listOfUserTypes as $usertype) {
            $user_types[] = array(
                'user_type_id' => $usertype->user_type_id,
                'user_type' => $usertype->user_type,
                'created_at' => date('d-m-Y', strtotime($usertype->created_at)),
                'updated_at' => date('d-m-Y', strtotime($usertype->updated_at))
            );
        }
        return view("admin.pages.usertypes", compact("user_types"));
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_type'    =>  'required'
        ]);

        $form_data = array(
            'user_type'       =>   $request->user_type
        );

        UserType::create($form_data);

        return redirect('/admin/usertypes')->with('success', 'Data Added successfully.');
    }

    public function edit($user_type_id)
    {
        $usertype_det = UserType::where('user_type_id', $user_type_id)->first()->getAttributes();
        return view('admin.pages.edit_usertype', compact('usertype_det'));
    }

    public function update(Request $request, $user_type_id)
    {

        $request->validate([
            'user_type'    =>  'required'
        ]);
        

        $form_data = array(
            'user_type'       =>   $request->user_type
            
        );

        UserType::whereUser_type_id($user_type_id)->update($form_data);

        return redirect('/admin/usertypes')->with('success', 'Data Updated successfully.');
    }

    public function destroy($user_type_id)
    {
        $data = UserType::findOrFail($user_type_id);
        $data->delete();

        return redirect('/admin/usertypes')->with('success', 'Data is successfully deleted');
    }
}
