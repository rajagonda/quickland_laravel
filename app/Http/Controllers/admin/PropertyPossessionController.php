<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PropertyPossession;
use View;

class PropertyPossessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_possessions = PropertyPossession::all();
        $possessions = [];
        foreach ($all_possessions as $possession) {
            $possessions[] = array(
                'possession_id' => $possession->possession_id,
                'possession' => $possession->possession,
                'created_at' => date('d-m-Y', strtotime($possession->created_at)),
                'updated_at' => date('d-m-Y', strtotime($possession->updated_at))
            );
        }
        return view("admin.pages.possessions", compact("possessions"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.add_property_possession');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'possession'    =>  'required'
        ]);

        $form_data = array(
            'possession'       =>   $request->possession
        );

        PropertyPossession::create($form_data);

        return redirect('/property/possessions')->with('success', 'Data Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($poss_id)
    {
        $poss_det = PropertyPossession::where('possession_id', $poss_id)->first()->getAttributes();
        return view('admin.pages.edit_property_possession', compact('poss_det'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $poss_id)
    {
        $request->validate([
            'possession'    =>  'required'
        ]);
        

        $form_data = array(
            'possession'       =>   $request->possession
            
        );

        PropertyPossession::wherePossession_id($poss_id)->update($form_data);

        return redirect('/property/possessions')->with('success', 'Data updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($poss_id)
    {
        $data = PropertyPossession::findOrFail($poss_id);
        $data->delete();

        return redirect('/property/possessions')->with('success', 'Data deleted successfully.');
    }
}
