<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PropertyUse;
use View;

class PropertyUsesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $propertyUses = PropertyUse::all();
        $property_uses = [];
        foreach ($propertyUses as $use) {
            $property_uses[] = array(
                'use_type_id' => $use->use_type_id,
                'use_type' => $use->use_type,
                'created_at' => date('d-m-Y', strtotime($use->created_at)),
                'updated_at' => date('d-m-Y', strtotime($use->updated_at))
            );
        }
        return view("admin.pages.propertyuses", compact("property_uses"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.add_property_use');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'use_type'    =>  'required'
        ]);

        $form_data = array(
            'use_type'       =>   $request->use_type
        );

        PropertyUse::create($form_data);

        return redirect('/property/uses')->with('success', 'Data Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($use_type_id)
    {
        $propertyuse_det = PropertyUse::where('use_type_id', $use_type_id)->first()->getAttributes();
        return view('admin.pages.edit_propertyuse', compact('propertyuse_det'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $use_type_id)
    {

        $request->validate([
            'use_type'    =>  'required'
        ]);
        

        $form_data = array(
            'use_type'       =>   $request->use_type
            
        );

        PropertyUse::whereUse_type_id($use_type_id)->update($form_data);

        return redirect('/property/uses')->with('success', 'Data Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($use_type_id)
    {
        $data = PropertyUse::findOrFail($use_type_id);
        $data->delete();

        return redirect('/property/uses')->with('success', 'Data deleted successfully.');
    }
}
