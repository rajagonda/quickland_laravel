<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PropertyAvailable;
use View;

class PropertyAvailabilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $available_for = PropertyAvailable::all();
        $availability = [];
        foreach ($available_for as $available) {
            $availability[] = array(
                'availability_id' => $available->availability_id,
                'available_for' => $available->available_for,
                'created_at' => date('d-m-Y', strtotime($available->created_at)),
                'updated_at' => date('d-m-Y', strtotime($available->updated_at))
            );
        }
        return view("admin.pages.property_availability", compact("availability"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.add_property_availability');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'available_for'    =>  'required'
        ]);

        $form_data = array(
            'available_for'       =>   $request->available_for
        );

        PropertyAvailable::create($form_data);

        return redirect('/property/availability')->with('success', 'Data Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($availability_id)
    {
        $availablefor_det = PropertyAvailable::where('availability_id', $availability_id)->first()->getAttributes();
        return view('admin.pages.edit_availablefor', compact('availablefor_det'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $availability_id)
    {
        $request->validate([
            'available_for'    =>  'required'
        ]);
        

        $form_data = array(
            'available_for'       =>   $request->available_for
            
        );

        PropertyAvailable::whereAvailability_id($availability_id)->update($form_data);

        return redirect('/property/availability')->with('success', 'Data updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($availability_id)
    {
        $data = PropertyAvailable::findOrFail($availability_id);
        $data->delete();

        return redirect('/property/availability')->with('success', 'Data deleted successfully.');
    }
}
