<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;


class UserController extends Controller
{
    public function showLogin()
    {
        // show the form

        return view('admin.pages.login');
    }

    public function doLogin(Request $request)
    {

        $rules = array(
            'email' => 'required',
            'password' => 'required'
        );
        // run the validation rules on the inputs from the form
        $userdata = $request->validate($rules);
        // create our user data for the authentication
        $userdata['is_admin']  = 1;
        DB::enableQueryLog();
        $user = DB::table('users')
                ->join('user_roles','users.id','=','user_roles.user_id')
                ->join('roles', 'roles.role_id','=','user_roles.role_id')
                ->where('roles.role_name','ADMIN')
            ->Where(function ($query) {
                /* In the email input field user can give phone number or email address. */
                $query->where('email', Input::get('email'));
                    //->orwhere('phone_number', Input::get('email'));
            })->get();

        if (count($user->all()) > 0) {
            $user_details = $user->all()[0];
            if (password_verify(Input::get('password'), $user_details->password)) {
                Session::put('username', $user_details->username);
                return redirect('admin/home');
            } else {
                return back()->withErrors(['auth_errors' => ['Invalid Credentials']]);
            }
        } else {
            return back()->withErrors(['auth_errors' => ['Invalid Credentials']]);
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('admin/login');
    }
}
