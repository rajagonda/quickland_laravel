<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AreaMeasures;
use View;

class AreaMeasuresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $areaMeasures = AreaMeasures::all();
        $area_measures = [];
        foreach ($areaMeasures as $measure) {
            $area_measures[] = array(
                'plot_measure_id' => $measure->plot_measure_id,
                'plot_measure' => $measure->plot_measure,
                'created_at' => date('d-m-Y', strtotime($measure->created_at)),
                'updated_at' => date('d-m-Y', strtotime($measure->updated_at))
            );
        }
        return view("admin.pages.plotareameasures", compact("area_measures"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.add_area_measure');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'plot_measure'    =>  'required'
        ]);

        $form_data = array(
            'plot_measure'       =>   $request->plot_measure
        );

        AreaMeasures::create($form_data);

        return redirect('/area/measures')->with('success', 'Data Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($plot_measure_id)
    {
        $measure_det = AreaMeasures::where('plot_measure_id', $plot_measure_id)->first()->getAttributes();
        return view('admin.pages.edit_area_measure', compact('measure_det'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $plot_measure_id)
    {
        $request->validate([
            'plot_measure'    =>  'required'
        ]);
        

        $form_data = array(
            'plot_measure'       =>   $request->plot_measure
            
        );

        AreaMeasures::wherePlot_measure_id($plot_measure_id)->update($form_data);

        return redirect('/area/measures')->with('success', 'Data Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($plot_measure_id)
    {
        $data = AreaMeasures::findOrFail($plot_measure_id);
        $data->delete();

        return redirect('/area/measures')->with('success', 'Data deleted successfully.');
    }
}
