<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PropertyType;
use View;

class PropertyTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $propertyTypes = PropertyType::all();
        $property_types = [];
        foreach ($propertyTypes as $proptype) {
            $property_types[] = array(
                'property_type_id' => $proptype->property_type_id,
                'property_type' => $proptype->property_type,
                'created_at' => date('d-m-Y', strtotime($proptype->created_at)),
                'updated_at' => date('d-m-Y', strtotime($proptype->updated_at))
            );
        }
        return view("admin.pages.propertytypes", compact("property_types"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.add_property_type');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'property_type'    =>  'required'
        ]);

        $form_data = array(
            'property_type'       =>   $request->property_type
        );

        PropertyType::create($form_data);

        return redirect('/property/types')->with('success', 'Data Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($property_type_id)
    {
        $propertytype_det = PropertyType::where('property_type_id', $property_type_id)->first()->getAttributes();
        return view('admin.pages.edit_propertytype', compact('propertytype_det'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $property_type_id)
    {

        $request->validate([
            'property_type'    =>  'required'
        ]);
        

        $form_data = array(
            'property_type'       =>   $request->property_type
            
        );

        PropertyType::whereProperty_type_id($property_type_id)->update($form_data);

        return redirect('/property/types')->with('success', 'Data Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($property_type_id)
    {
        $data = PropertyType::findOrFail($property_type_id);
        $data->delete();

        return redirect('/property/types')->with('success', 'Data Updated successfully.');
    }
}
