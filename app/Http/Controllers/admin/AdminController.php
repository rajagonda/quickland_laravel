<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use View;

class AdminController extends Controller
{
	//

	public function home()
	{

		if (Session::has('username') && Session::get('username') == 'admin') {
			return view('admin.pages.home');
		} else {
			return redirect('admin/login');
		}
	}
}
