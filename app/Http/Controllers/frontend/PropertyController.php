<?php

namespace App\Http\Controllers\frontend;

use App\Models\Availability;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserType;
use App\Models\PropertyUse;
use App\Models\PropertyType;
use App\Models\PropertySubType;
use App\Models\PropertyAvailable;
use App\Models\PropertyPossession;
use App\Models\AreaMeasures;
use App\Models\State;
use App\Models\City;
use App\Models\Property;
use File;
use DB;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class PropertyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cities = City::all();
        if ($request->input('city_id')) {
            $data = Property::where('city_id', $request->input('city_id'))->paginate(3);
            return view('frontend.pages.property_list', compact('data'))
                ->with('i', ($request->input('page', 1) - 1) * 3)
                ->with('cities', $cities)
                ->with('city_id', $request->input('city_id'));
        } else {
            $data = Property::paginate(3);
            return view('frontend.user.property.property_list', compact('data'))
//                ->with('i', ($request->input('page', 1) - 1) * 3)
                ->with('cities', $cities);
        }

    }

    public function user_prop_list(Request $request)
    {

        $data = Property::where('user_id', session()->get('user_id'))->paginate(3);
        return view('frontend.user.property.property_list', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 3);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


        $data = array();

        //
        $data['user_types'] = UserType::all();

        $data['property_uses'] = PropertyUse::all();


        $data['property_types'] = PropertyType::all();


        $data['availability'] = Availability::all();


        $data['possessions'] = PropertyPossession::all();


        $data['measures'] = AreaMeasures::all();


        $data['states'] = State::all();
        $data['active_opt'] = $request->step;


        return view('frontend.user.property.add_property', $data);
    }

    public function manage(Request $request)
    {


        $data = array();

        //
        $data['user_types'] = UserType::all();

        $data['property_uses'] = PropertyUse::all();


        $data['property_types'] = PropertyType::all();


        $data['availability'] = Availability::all();


        $data['possessions'] = PropertyPossession::all();


        $data['measures'] = AreaMeasures::all();


        $data['states'] = State::all();
        $data['property_id'] = $request->id;
        $data['property_info'] = Property::where('id', $request->id)->first();
        $data['active_opt'] = $request->step;


        return view('frontend.user.property.manage_property', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*request()->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);*/


        if ($request->isMethod('post')) {

            $inputData = $request->all();


            switch ($inputData['post_actions']) {
                case "general_info":

                    if (isset($inputData['possession_date'])) {

                        $inputData['possession_date'] = date('Y-m-d', strtotime($inputData['possession_date']));
                    }
//                    $inputData['property_id']= uniqid('id_', 10);
                    $inputData['property_id'] = Str::random(10);

//                    dd($inputData);


                    $insertItem = Property::create($inputData);


                    return redirect()->route('ajax.user.property.manage', ['id' => $insertItem->id, 'step' => 'location']);

                    break;
            }


//            $property_details = $this->fill_property_details($input);
//            // var_dump($property_details); exit;
//            $images = array();
//            if ($files = $request->file('prop_photos')) {
//                $dir_name = $this->generate_unique_prop_dir_to_upload_photos();
//                if (!File::exists(public_path() . "/property_photos/$dir_name")) {
//                    Storage::makeDirectory(public_path() . "/property_photos/$dir_name");
//                }
//                foreach ($files as $file) {
//                    $name = $file->getClientOriginalName();
//                    $file->move(public_path() . "/property_photos/$dir_name", $name);
//                    $images[] = $name;
//                }
//            }
//            $property_details['old_property_files'] = implode(',', $images);
//            $property_details['new_property_files'] = implode(',', $images);
//            $property_details['property_id'] = $dir_name;
//
//            Property::create($property_details);
//            return back()->with('success', 'You have successfully added property details.');


        } else {
            return redirect()->route('ajax.user.property.add');
        }


    }

    public function manage_process(Request $request)
    {
        /*request()->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);*/


        if ($request->isMethod('post')) {

            $inputData = $request->all();

            switch ($inputData['post_actions']) {
                case "general_info":

                    if (isset($inputData['possession_date'])) {

                        $inputData['possession_date'] = date('Y-m-d', strtotime($inputData['possession_date']));
                    }
//                    $inputData['property_id']= uniqid('id_', 10);

//                    dd($inputData);
//                    unset($inputData['_token']);

//                    dd($inputData);
                    $propertyData = Property::where('id', $inputData['current_property_id'])->first();
//                    dd($propertyData);

                    $propertyData->update($inputData);


                    return redirect()->route('ajax.user.property.manage', ['id' => $propertyData->id, 'step' => 'location']);

                    break;

                case 'location':

                    $propertyData = Property::where('id', $inputData['current_property_id'])->first();
//                    dd($propertyData);
                    $propertyData->update($inputData);


                    return redirect()->route('ajax.user.property.manage', ['id' => $propertyData->id, 'step' => 'property_info']);


                    break;
            }


//            $property_details = $this->fill_property_details($input);
//            // var_dump($property_details); exit;
//            $images = array();
//            if ($files = $request->file('prop_photos')) {
//                $dir_name = $this->generate_unique_prop_dir_to_upload_photos();
//                if (!File::exists(public_path() . "/property_photos/$dir_name")) {
//                    Storage::makeDirectory(public_path() . "/property_photos/$dir_name");
//                }
//                foreach ($files as $file) {
//                    $name = $file->getClientOriginalName();
//                    $file->move(public_path() . "/property_photos/$dir_name", $name);
//                    $images[] = $name;
//                }
//            }
//            $property_details['old_property_files'] = implode(',', $images);
//            $property_details['new_property_files'] = implode(',', $images);
//            $property_details['property_id'] = $dir_name;
//
//            Property::create($property_details);
//            return back()->with('success', 'You have successfully added property details.');


        } else {
            return redirect()->route('ajax.user.property.add');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($property_id, $type)
    {
        $data = Property::where('property_id', $property_id)
            ->leftJoin('property_types', 'properties.property_type_id', '=', 'property_types.property_type_id')
            ->leftJoin('property_uses', 'properties.use_type_id', '=', 'property_uses.use_type_id')
            ->leftJoin('plot_area_measures', 'properties.plot_area_measure_id', '=', 'plot_area_measures.plot_measure_id')
            ->leftJoin('states', 'states.state_id', '=', 'properties.state_id')
            ->leftJoin('cities', 'cities.city_id', '=', 'properties.city_id')
            ->select('properties.*', 'property_uses.use_type', 'plot_area_measures.plot_measure', 'states.state', 'cities.city', 'property_types.property_type')
            ->first();

        $prop_details = $data;

        if ($type == 1) {

        } else {
            return view('frontend.pages.show_property_two', compact("prop_details"));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generate_unique_prop_dir_to_upload_photos()
    {
        $randomid = mt_rand(100000, 999999);
        $property_id = "PPT-" . $randomid;
        while (1) {
            $properties = Property::where('property_id', $property_id)->get()->toArray();

            if (count($properties) == 0) {
                return $property_id;
            }
            $randomid = mt_rand(100000, 999999);
            $property_id = "PPT-" . $randomid;

        }

    }

    public function enquiry($property_id, Request $request)
    {
        $input = $request->all();

        $enq_data = [];
        foreach ($input as $key => $val) {
            if ($key != '_token') {
                $enq_data[$key] = $val;
            }
        }
        $enq_data['property_id'] = $property_id;
        $enq_data['created_at'] = date('Y-m-d H:i:s');
        DB::table('property_enquiries')->insert($enq_data);
        echo 1;
    }

    private function fill_property_details($input)
    {
        $property_details = [];
        $property_details['user_type_id'] = isset($input['user_type']) ? $input['user_type'] : NULL;
        $property_details['user_id'] = session()->get('user_id');
        $property_details['property_type_id'] = isset($input['property_type']) ? $input['property_type'] : NULL;
        $property_details['use_type_id'] = isset($input['use_type']) ? $input['use_type'] : NULL;
        $property_details['video_url'] = isset($input['video_url']) ? $input['video_url'] : NULL;
        $property_details['property_sub_type_id'] = isset($input['property_sub_type']) ? $input['property_sub_type'] : NULL;
        $property_details['availability_id'] = isset($input['available_for']) ? $input['available_for'] : NULL;
        $property_details['possession_id'] = isset($input['possession']) ? $input['possession'] : NULL;
        $property_details['possession_date'] = isset($input['possession_date']) ? $input['possession_date'] : NULL;
        $property_details['state_id'] = isset($input['state_id']) ? $input['state_id'] : NULL;
        $property_details['city_id'] = isset($input['city_id']) ? $input['city_id'] : NULL;
        $property_details['project_name'] = isset($input['project_name']) ? $input['project_name'] : NULL;
        $property_details['locality'] = isset($input['locality']) ? $input['locality'] : NULL;
        $property_details['property_address'] = isset($input['property_address']) ? $input['property_address'] : NULL;
        $property_details['plot_area'] = isset($input['plot_area']) ? $input['plot_area'] : NULL;
        $property_details['plot_area_measure_id'] = isset($input['plot_area_measure_id']) ? $input['plot_area_measure_id'] : NULL;
        $property_details['no_of_plots_per_floor'] = isset($input['no_of_plots_per_floor']) ? $input['no_of_plots_per_floor'] : NULL;
        $property_details['length'] = isset($input['length']) ? $input['length'] : NULL;
        $property_details['length_measure_id'] = isset($input['length_measure_id']) ? $input['length_measure_id'] : NULL;
        $property_details['width'] = isset($input['width']) ? $input['width'] : NULL;
        $property_details['width_measure_id'] = isset($input['width_measure_id']) ? $input['width_measure_id'] : NULL;
        $property_details['width_of_facing'] = isset($input['width_of_facing']) ? $input['width_of_facing'] : NULL;
        $property_details['facing_measure_id'] = isset($input['facing_measure_id']) ? $input['facing_measure_id'] : NULL;
        $property_details['plot_price'] = isset($input['plot_price']) ? $input['plot_price'] : NULL;
        $property_details['price_per_sq_feet'] = isset($input['price_per_sq_feet']) ? $input['price_per_sq_feet'] : NULL;
        $property_details['plot_booking_amount'] = isset($input['plot_booking_amount']) ? $input['plot_booking_amount'] : NULL;
        $property_details['maintenance_amount'] = isset($input['maintenance_amount']) ? $input['maintenance_amount'] : NULL;
        $property_details['booking_type'] = isset($input['booking_type']) ? $input['booking_type'] : NULL;
        $property_details['about_property'] = isset($input['about_property']) ? $input['about_property'] : NULL;
        $property_details['about_you'] = isset($input['about_you']) ? $input['about_you'] : NULL;
        $property_details['amenities'] = implode(',', $input['amenities']);
        $property_details['package'] = $input['package'];
        $property_details['negotiable'] = isset($input['negotiable']) ? $input['negotiable'] : 0;
        return $property_details;
    }
}
