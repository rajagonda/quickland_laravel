<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserType;
use App\Models\PropertyUse;
use App\Models\PropertyType;
use App\Models\PropertySubType;
use App\Models\PropertyAvailable;
use App\Models\PropertyPossession;
use App\Models\AreaMeasures;
use App\Models\State;
use App\Models\City;
use App\Models\Property;
use Illuminate\Support\Facades\Auth;

class FrontController extends Controller
{


    //
    public function home()
    {


//        $data = Property::orderBy('created_at', 'DESC')
//            ->leftJoin('property_types', 'properties.property_type_id', '=', 'property_types.property_type_id')
//            ->leftJoin('property_uses', 'properties.use_type_id', '=', 'property_uses.use_type_id')
//            ->leftJoin('plot_area_measures', 'properties.plot_area_measure_id', '=', 'plot_area_measures.plot_measure_id')
//            ->leftJoin('states', 'states.state_id', '=', 'properties.state_id')
//            ->leftJoin('cities', 'cities.city_id', '=', 'properties.city_id')
//            ->select('properties.*', 'property_uses.use_type', 'plot_area_measures.plot_measure', 'states.state', 'cities.city', 'property_types.property_type')
//            ->select('properties.*', 'property_uses.use_type', 'plot_area_measures.plot_measure', 'states.state', 'cities.city', 'property_types.property_type')
//            ->select('properties.*', 'property_uses.use_type', 'plot_area_measures.plot_measure', 'states.state', 'cities.city', 'property_types.property_type')
//            ->get();

        $data = Property::orderBy('created_at', 'DESC')
            ->get();

//		$raaa= 'asdas';
//		$raaa = 'sssss';


        return view('frontend.pages.home', compact('data'));

    }


    public function logout()
    {
        Auth::logout();
        return redirect()->route('site.home');

    }

}
