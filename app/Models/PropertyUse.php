<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyUse extends Model
{
    //
    protected $table = 'property_uses';
    protected $primaryKey = 'use_type_id';
    protected $fillable = [
        'use_type'
       ];
}
