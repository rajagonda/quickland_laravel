<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyType extends Model
{
    //
    protected $table = 'property_types';
    protected $primaryKey = 'id';
    protected $fillable = [
        'property_type'
       ];
}
