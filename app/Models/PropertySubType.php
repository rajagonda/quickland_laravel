<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertySubType extends Model
{
    //
    protected $table = 'property_sub_types';
    protected $primaryKey = 'id';
    protected $fillable = [
        'property_sub_type'
    ];
}
