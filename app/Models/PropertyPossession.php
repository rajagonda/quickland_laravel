<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyPossession extends Model
{
    protected $table = 'possessions';
    protected $primaryKey = 'possession_id';
    protected $fillable = [
        'possession'
       ];
}
