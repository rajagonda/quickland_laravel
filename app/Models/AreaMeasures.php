<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AreaMeasures extends Model
{
    //
    protected $table = 'plot_area_measures';
    protected $primaryKey = 'plot_measure_id';
    protected $fillable = [
        'plot_measure'
       ];
}
