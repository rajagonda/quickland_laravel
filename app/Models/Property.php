<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
//    protected $table = 'properties';
//    protected $primaryKey = 'id';
    protected $fillable = [
        'property_id',

        'user_type_id',
        'about_you',
        'use_type_id',
        'property_type_id',
        'property_sub_type_id',
        'availability_id',
        'possession_id',
        'possession_date',

        'locality',
        'state_id',
        'city_id',
        'project_name',
        'property_address',



		'old_property_files',
		'new_property_files',





		'plot_area',
		'plot_area_measure_id',
		'no_of_plots_per_floor',
		'length',
		'length_measure_id',
		'width',
		'width_measure_id',
		'width_of_facing',
		'facing_measure_id',
		'plot_price',
		'price_per_sq_feet',
		'plot_booking_amount',
		'maintenance_amount',
		'booking_type',
		'about_property',
		'amenities',

        'user_id',
        'video_url',

        'package'
       ];
}
