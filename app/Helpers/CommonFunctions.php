<?php

use App\Models\Countries;
use App\Models\Products;

define('ADMIN_EMAIL', 'info@idconnect.com');


function generateNumericOTP($n)
{

    // Take a generator string which consist of
    // all numeric digits
    $generator = "1357902468";

    // Iterate for n-times and pick a single character
    // from generator and append it to $result

    // Login for generating a random character from generator
    //     ---generate a random number
    //     ---take modulus of same with length of generator (say i)
    //     ---append the character at place (i) from generator to result

    $result = "";

    for ($i = 1; $i <= $n; $i++) {
        $result .= substr($generator, (rand() % (strlen($generator))), 1);
    }

    // Return result
    return $result;
}


function sendSms($no, $msg, $country = 91)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.msg91.com/api/sendhttp.php");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "mobiles=$no&authkey=281207Ap4gZfODekY5d05c521&route=4&sender=VDESIC&message=$msg&country=$country");
//    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=sonygolden&password=info1234&to=$no&from=SGSCHT&message=$msg");
    $buffer = curl_exec($ch);

    if (empty($buffer)) {
        echo "buffer is empty";
    }

    curl_close($ch);
}


function propertyOptions($opt = null, $opt1 = null, $opt2 = null)
{
    $returndata = array();

    $data = array(
        'lead' => array(
            'sale',
            'buy',
        ),
        'type' => array(
            1 => array(
                'info' => array(
                    'name' => 'Residential Project',
                    'alias' => 'residential',
                    'id' => 1,
                ),
                'opt' => array(
                    'residential_individual_plot',

                ),
            ),

            2 => array(
                'info' => array(
                    'name' => 'Commercial Project',
                    'alias' => 'commercial',
                    'id' => 1,
                ),
                'opt' => array(),
            ),


        )
    );
//    $data['lead']= '';


    if ($opt != '' && isset($data[$opt])) {


        if ($opt1 != '' && isset($data[$opt][$opt1])) {


//            if ($opt2 != '' && isset($data[$opt][$opt1][$opt2])) {
//
//                $returndata = $data[$opt][$opt1][$opt2];
//
//            } else {
//                $returndata = $data[$opt][$opt1];
//            }

            $returndata = $data[$opt][$opt1];


        } else {
            $returndata = $data[$opt];
        }


    } else {
        $returndata = $data;
    }

    return $returndata;
}


//ALL SMS 9032615531
define('MSG_ADMIN_NUMBER', '8977111142');
//define('MSG_ADMIN_NUMBER', '8977111142');
define('MSG_USER_REGISTER_NOTFICATION_USER_OTP', 'Quick Land: Dear {USERNAME}, you have registered successfully. Please varify your Account OTP {OTP}.');

//define('MSG_USER_REGISTER_NOTFICATION_ADMIN', '{USER_NAME} Registered with you as User');
//define('MSG_USER_REGISTER_NOTFICATION_USER', 'Dear Vdesiconnect Customer, you have registered successfully. Please check your email for verification link.');
//
//
//define('MSG_VENDOR_REGISTER_NOTFICATION_ADMIN', '{USER_NAME} Registered with you as Vdesiconnect Vendor');
//define('MSG_VENDOR_REGISTER_NOTFICATION_VENDOR', 'Dear Vdesiconnect Vendor, you have registered successfully. Please check your email for verification link.');
//
//
//define('MSG_SERVICE_REQUEST_ADMIN', 'Dear Vdesiconnect Admin, You received a "{SERVICE_NAME}" enquiry from "{USER_NAME}". Please review the request.');
//define('MSG_SERVICE_REQUEST_USER', 'Dear Vdesiconnect Customer, we received enquiry for "{SERVICE_NAME}". We will review and get back to you soon.');
//
//define('MSG_SERVICE_INVOICE_CREATE_USER', 'Dear Vdesiconnect Customer, you have received incovice for {SERVICE_NAME}. Please login to vdeisconnect.com to make payment.');
//
//define('MSG_PRODUCT_ENQUERY_ACCEPT_USER', 'Dear Vdesiconnect Customer, your enquiry for "{PRODUCT_NAME}" is received. We have the product available please use this OTP {OTP} number to purchase. visit to to https://www.vdesiconnect.com');
//define('MSG_PRODUCT_ENQUERY_ACCEPT_VENDOR', 'Dear Vdesiconnect Vendor, you received an enquiry for "{PRODUCT_NAME}". Please check and notify Vdesiconnect Admin on the availability of the product.');
//define('MSG_PRODUCT_ENQUERY_ACCEPT_ADMIN', 'Dear Vdesiconnect Admin, you received an enquiry by {USER_NAME} for "{PRODUCT_NAME}". Please check the availability with Vendor.');
//
//define('MSG_PRODUCT_ENQUERY_REJECT_USER', 'Dear Vdesiconnect Customer, the "{PRODUCT_NAME}" you enquired is not available. Check Vdesiconnect.com for more products. ({ADMINMSG})');
//
//
//define('MSG_CART_PAYMENT_DONE_USER', 'Dear Vdesiconnect Customer, your order for {PRODUCT_NAME} is successfully placed. Your Order id is #{ORDER_ID}. Login to vdesiconnect.com for checking the order status.');
//define('MSG_CART_PAYMENT_DONE_VENDOR', 'Dear Vdesiconnect Vendor, you got an order for {PRODUCT_NAME}. Order id is #{ORDER_ID}. For more details please, login to https://www.vdesiconnect.com/store/login.');
//define('MSG_CART_PAYMENT_DONE_ADMIN', 'Dear Vdesiconnect Admin, you got an order for {PRODUCT_NAME}. Order id is #{ORDER_ID}. For more details please, login to https://www.vdesiconnect.com/admin/login.');


//'https://api.msg91.com/api/sendhttp.php?mobiles=9642123254,7995165789&authkey=281207Ap4gZfODekY5d05c521&route=4&sender=VDESIC&message=Hello!%20This%20is%20a%20test%20message&country=91';

