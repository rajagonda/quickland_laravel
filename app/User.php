<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    //
    protected $table = 'users';

    protected $fillable = [
        'name',
        'email',
        'password',
        'phone_number',
        'is_admin',

        'mobile_verified_at',
        'email_verified_at',
        'status',
    ];

    protected $hidden = [
        'password', 'remember_token', 'varify_otp',
    ];


}
