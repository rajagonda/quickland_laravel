<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
	return view('welcome');
});*/
Route::get('/', 'frontend\FrontController@home')->name('site.home');

Route::prefix('property')->group(function () {
    Route::get('/list', 'frontend\PropertyController@index')->name('site.property.list');
    Route::get('/show/{property_id}/{type}', 'frontend\PropertyController@show')->name('site.property.show');
    Route::post('/enquiry/{property_id}/', 'frontend\PropertyController@enquiry')->name('site.property.enquiry');


});


Route::prefix('ajax')->group(function () {
    Route::get('/ajax/get_cities_of_state/{state_id}', 'frontend\AjaxController@getCitiesOfState')->name('ajax.cities.get');

    Route::post('/user/register', 'AjaxController@registerAjax')->name('site.ajax.user.rigister');
    Route::post('/user/register/otp', 'AjaxController@registerOtpVarify_Ajax')->name('site.ajax.user.rigister.otp_varify');
    Route::post('/login', 'AjaxController@loginAjax')->name('site.ajax.user.login');

    Route::prefix('property')->group(function () {

        Route::post('/get-opt', 'AjaxController@propertyOpts')->name('site.ajax.property.opt');

    });
});


Route::prefix('user')->group(function () {

    Route::prefix('property')->group(function () {

//        Route::get('/add', 'frontend\PropertyController@create')->name('site.property.add');
//        Route::post('/store', 'frontend\PropertyController@store')->name('site.property.edit');

        Route::get('/property/list', 'frontend\PropertyController@user_prop_list')->name('site.user.property.list');


        Route::get('/property/add/{step?}', 'frontend\PropertyController@create')->name('ajax.user.property.add');
        Route::match(['GET', 'POST'], '/store', 'frontend\PropertyController@store')->name('ajax.user.property.store');

        Route::get('/property/manage/{id}/{step?}', 'frontend\PropertyController@manage')->name('ajax.user.property.manage');

        Route::match(['GET', 'POST'], '/{id}/manage_process', 'frontend\PropertyController@manage_process')->name('ajax.user.property.manage_process');



    });


    Route::post('/register', 'frontend\UserController@register')->name('site.user.rigister_process');
//    Route::post('/register', 'frontend\UserController@register')->name('site.user.rigister_process');


    Route::get('/logout', 'frontend\FrontController@logout')->name('site.user.logout_process');
});


//Route::group(['middleware' => 'usersession'], function () {
//
//});


Route::prefix('admin')->group(function () {
    Route::get('/login', 'admin\UserController@showLogin')->name('site.loginform');
    Route::post('/dologin', 'admin\UserController@doLogin')->name('site.login_process');

    Route::get('/home', 'admin\AdminController@home');
    Route::get('/logout', 'admin\UserController@logout');

    Route::get('/usertypes', 'admin\UserTypesController@listUserTypes');


    Route::get('/usertype/add', 'admin\UserTypesController@create');
    Route::post('/usertype/store', 'admin\UserTypesController@store');
    Route::get('/usertype/edit/{user_type_id}', 'admin\UserTypesController@edit');
    Route::post('/usertype/update/{user_type_id}', 'admin\UserTypesController@update');
    Route::get('/usertype/delete/{user_type_id}', 'admin\UserTypesController@destroy');


    Route::prefix('property')->group(function () {
        Route::get('/types', 'admin\PropertyTypesController@index');
        Route::get('/type/add', 'admin\PropertyTypesController@create');
        Route::post('/type/store', 'admin\PropertyTypesController@store');
        Route::get('/type/edit/{property_type_id}', 'admin\PropertyTypesController@edit');
        Route::post('/type/update/{property_type_id}', 'admin\PropertyTypesController@update');
        Route::get('/type/delete/{property_type_id}', 'admin\PropertyTypesController@destroy');

        Route::get('/uses', 'admin\PropertyUsesController@index');


        Route::get('/availability', 'admin\PropertyAvailabilityController@index');
        Route::get('/availability/add', 'admin\PropertyAvailabilityController@create');
        Route::post('/availability/store', 'admin\PropertyAvailabilityController@store');
        Route::get('/availability/edit/{availability_id}', 'admin\PropertyAvailabilityController@edit');
        Route::post('/availability/update/{availability_id}', 'admin\PropertyAvailabilityController@update');
        Route::get('/availability/delete/{availability_id}', 'admin\PropertyAvailabilityController@destroy');

        Route::get('/possessions', 'admin\PropertyPossessionController@index');
        Route::get('/possession/add', 'admin\PropertyPossessionController@create');
        Route::post('/possession/store', 'admin\PropertyPossessionController@store');
        Route::get('/possession/edit/{possession_id}', 'admin\PropertyPossessionController@edit');
        Route::post('/possession/update/{possession_id}', 'admin\PropertyPossessionController@update');
        Route::get('/possession/delete/{possession_id}', 'admin\PropertyPossessionController@destroy');

    });

    Route::get('/propertyuse/add', 'admin\PropertyUsesController@create');
    Route::post('/propertyuse/store', 'admin\PropertyUsesController@store');
    Route::get('/propertyuse/edit/{use_type_id}', 'admin\PropertyUsesController@edit');
    Route::post('/propertyuse/update/{use_type_id}', 'admin\PropertyUsesController@update');
    Route::get('/propertyuse/delete/{use_type_id}', 'admin\PropertyUsesController@destroy');


    Route::get('/area/measures', 'admin\AreaMeasuresController@index');
    Route::get('/area/measure/add', 'admin\AreaMeasuresController@create');
    Route::post('/area/measure/store', 'admin\AreaMeasuresController@store');
    Route::get('/area/measure/edit/{plot_measure_id}', 'admin\AreaMeasuresController@edit');
    Route::post('/area/measure/update/{plot_measure_id}', 'admin\AreaMeasuresController@update');
    Route::get('/area/measure/delete/{plot_measure_id}', 'admin\AreaMeasuresController@destroy');


});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
